KEYWORDS="TODO:|FIXME:|HACK:|BUG:|\?\?\?:|\!\!\!:"

find "${SRCROOT}" \( -name "*.h" -or -name "*.m" -or -name "*.swift" \) -not -path "${SRCROOT}/Carthage/*" -print0 | xargs -0 egrep --with-filename --line-number --only-matching "($KEYWORDS).*\$" | perl -p -e "s/($KEYWORDS)/ warning: \$1/"
