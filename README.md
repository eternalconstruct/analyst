# ANALYST  
  
Master:  [![build status](https://gitlab.com/inslayninteractive/analyst/badges/master/build.svg)](https://gitlab.com/inslayninteractive/analyst/commits/master)  
Develop: [![build status](https://gitlab.com/inslayninteractive/analyst/badges/develop/build.svg)](https://gitlab.com/inslayninteractive/analyst/commits/develop)  
Code Coverage: [![codecov](https://codecov.io/gl/inslayninteractive/analyst/branch/develop/graph/badge.svg)](https://codecov.io/gl/inslayninteractive/analyst)  
Code Health:  [![codebeat badge](https://codebeat.co/badges/9cd94d10-f6f9-4a04-a37f-a1b1f4a986b8)](https://codebeat.co/projects/gitlab-com-inslayninteractive-analyst)  
[![Carthage compatible](https://img.shields.io/badge/Carthage-compatible-4BC51D.svg?style=flat)](https://github.com/Carthage/Carthage)  
[comment]: ([![CocoaPods](https://img.shields.io/cocoapods/v/Analyst.svg?maxAge=2592000)]())
