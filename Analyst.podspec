Pod::Spec.new do |spec|
  spec.name = "Analyst"
  spec.version = "1.0.0"
  spec.summary = "A framework to make interacting with the AniList API easier on iOS."
  spec.homepage = "http://analyst.inslayn.com"
  spec.license = { type: 'MIT', file: 'LICENSE' }
  spec.authors = { "Nick McVroom-Amoakohene" => 'nick@inslayn.com' }
  spec.social_media_url = "http://twitter.com/inslaynstudio"

  spec.platform = :ios, "9.1"
  spec.requires_arc = true
  spec.source = { git: "https://gitlab.com/inslayninteractive/analyst.git", tag: "v#{spec.version}", submodules: true }
  spec.source_files = "Analyst/**/*.{h,swift}"

 # spec.dependency = ""
end
