//
//  ViewController.swift
//  AnalystApp
//
//  Created by Nick McVroom-Amoakohene on 15/05/2016.
//  Copyright © 2016 inslayn interactive. All rights reserved.
//

import UIKit
import Analyst

class ViewController: UIViewController {
    @IBOutlet weak var testSelectedAnime: UIButton?
    @IBOutlet weak var testSuggestions: UIButton?
    
    @IBOutlet weak var txtSelectedAnime: UITextField?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        AnalystService.onError = { title, message in
            let alert = UIAlertController(title: title, message: message, preferredStyle: .Alert)
            
            self.presentViewController(alert, animated: true, completion: nil)
        }
        
        let config: AnalystConfig
        
        if  let info        = NSBundle.mainBundle().infoDictionary,
            let settings    = info["Settings"] as! Dictionary<String, AnyObject>?
        {
            
            if  let id      = settings["ID"] as? String,
                let secret  = settings["Secret"] as? String,
                let url     = settings["URL"] as? String
            {
                config = AnalystConfig(clientId: id, clientSecret: secret, apiUrl: url)
                
                if let redirect = settings["Redirect"] as? String { config.redirectUrl = redirect }
                if let debug    = settings["Debug"] as? Bool { config.debug = debug }
                if let interval = settings["Data Refresh Interval"] as? NSTimeInterval { config.refreshInterval = interval }
                
                do {
                    try AnalystService.initialise(withConfig: config)
                }
                catch InitialisationError.APIUrl(let message) {
                    print(message)
                }
                catch InitialisationError.ClientID(let message) {
                    print(message)
                }
                catch InitialisationError.ClientSecret(let message) {
                    print(message)
                }
                catch InitialisationError.Failed(let message) {
                    print(message)
                }
                catch {
                    print("An unknown initialisation error occurred.")
                }
            }
            else {
                print("Something went wrong! Check settings and make sure the ID, URL and Secret are correctly set.")
            }
        }
        else {
            print("Something went wrong! Check Settings exist in Info.plist")
        }
        
        do {
            try AnalystService.authenticateAsGuest()
        }
        catch AuthorisationError.GrantType(let message) {
            print(message)
        }
        catch {
            print("An unknown authorisation error occurred.")
        }
    }
    
    @IBAction func resetStoredTokens() {
        AnalystService.clearAuthentication()
        
        do {
            try AnalystService.authenticateAsGuest()
        }
        catch AuthorisationError.GrantType(let message) {
            print(message)
        }
        catch {
            print("An unknown authorisation error occurred.")
        }
    }
    
    @IBAction func authorise() {
        do {
            try AnalystService.authenticateWithCode()
        }
        catch AuthorisationError.GrantType(let message) {
            print(message)
        }
        catch {
            print("An unknown authorisation error occurred.")
        }
    }
    
    @IBAction func query() {
        AnalystService.query("status=\(Series.AiringStatus.NotAired)d&genres=Ecchi,Comedy,Seinen&airing_data=true&full_page=true", endpoint: "browse/\(Series.SeriesType.Anime)")
        {
            seriesList in
            for anime in seriesList {
                print("Query result: \(anime.titles?.Japanese) [\(anime.titles?.English)]")
            }
        }
    }
    
    @IBAction func currentlyAiring() {
        AnalystService.currentlyAiringAnime() {
            seriesList in
            for anime in seriesList {
                print("Currently Airing: \(anime.titles?.Japanese) [\(anime.titles?.English)]")
            }
        }
    }
    
    @IBAction func currentlyAiringToday() {
        AnalystService.animeAiringToday() {
            seriesList in
            for anime in seriesList {
                print("Currently Airing Today: \(anime.titles?.Japanese) [\(anime.titles?.English)]")
                if let airing = anime.airingStats {
                    if let time = airing.time {
                        do {
                            print("\(time), \(try time.toDate()))")
                        }
                        catch {
                            
                        }
                    }
                }
            }
        }
    }
    
    @IBAction func suggestedAnime() {
        if txtSelectedAnime?.text != "" {
            AnalystService.series((txtSelectedAnime?.text)!, seriesType: Series.SeriesType.Anime) {
                anime in
                guard let selected = anime else { print("No anime found with id \(self.txtSelectedAnime?.text)"); return }
                
                AnalystService.suggestions(selected, seriesType: Series.SeriesType.Anime) { list in
                    for each in list {
                        print("You might like: \(each.titles?.Romaji) [\(each.titles?.English)]")
                    }
                }
            }
        }
    }
    
    @IBAction func selectedAnime() {
        if txtSelectedAnime?.text != "" {
            AnalystService.series((txtSelectedAnime?.text)!, seriesType: Series.SeriesType.Anime) {
                anime in
                guard let selected = anime else { print("No anime found with id \(self.txtSelectedAnime?.text)"); return }
                
                print("You selected: \(selected.titles?.Romaji) [\(selected.titles?.English)]")
            }
        }
    }
    
    @IBAction func springAnime() {
        AnalystService.seriesForSeason(.Spring, seriesType: Series.SeriesType.Anime) { list in
            for each in list {
                print("\(Analyst.Season.Spring.rawValue): \(each.titles?.Romaji) [\(each.titles?.English)]")
            }
        }
    }
    
    @IBAction func summerAnime() {
        AnalystService.seriesForSeason(.Summer, seriesType: Series.SeriesType.Anime) { list in
            for each in list {
                print("\(Analyst.Season.Summer.rawValue): \(each.titles?.Romaji) [\(each.titles?.English)]")
            }
        }
    }
    
    @IBAction func autumnAnime() {
        // Can also use .Fall, but I'm British ;)
        AnalystService.seriesForSeason(.Autumn, seriesType: Series.SeriesType.Anime) { list in
            for each in list {
                print("\(Analyst.Season.Autumn.rawValue): \(each.titles?.Romaji) [\(each.titles?.English)]")
            }
        }
    }
    
    @IBAction func winterAnime() {
        AnalystService.seriesForSeason(.Winter, seriesType: Series.SeriesType.Anime) { list in
            for each in list {
                print("\(Analyst.Season.Winter.rawValue): \(each.titles?.Romaji) [\(each.titles?.English)]")
            }
        }
    }
    
    @IBAction func upcomingAnime() {
        AnalystService.seriesForUpcomingSeason(Series.SeriesType.Anime) { list in
            for each in list {
                print("Upcoming: \(each.titles?.Romaji) [\(each.titles?.English)] (\(each.airingStats?.time))")
            }
        }
    }
    
    @IBAction func selectedAnimeChanged() {
        let enabled = txtSelectedAnime?.text != ""
        
        testSelectedAnime?.enabled = enabled
        testSuggestions?.enabled = enabled
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

