//
//  AniListAuthResponse.swift
//  Analyst
//
//  Created by Nick McVroom-Amoakohene on 31/07/2016.
//  Copyright © 2016 inslayn interactive. All rights reserved.
//

import SwiftyJSON

public enum AniListGrantType: String {
    case ClientCredentials  = "client_credentials"
    case PIN                = "authorization_pin"
    case Code               = "authorization_code"
    case Refresh            = "refresh_token"
    case Unknown            = "unknown"
}

public struct AniListAuthResponse {
    public private(set) var accessToken: String?
    public private(set) var tokenType: String?
    public private(set) var expires: Int?
    public private(set) var expiresIn: Int?
    public private(set) var refreshToken: String?
    
    let ACCESS_TOKEN_KEY    = "analyst_accessToken"
    let TOKEN_TYPE_KEY      = "analyst_tokenType"
    let EXPIRES_KEY         = "analyst_expires"
    let EXPIRES_IN_KEY      = "analyst_expiresIn"
    let REFRESH_TOKEN_KEY   = "analyst_refreshToken"
    
    init?() { }
    
    init?(json responseDict: JSON) {
        if responseDict.count == 0 {
            return nil
        }
        
        for (key, value): (String, JSON) in responseDict {
            log(message: "Retrieved [\(key)] with value [\(value)]", level: .Authorisation)
        }
        
        for (key, value): (String, JSON) in responseDict {
            log(message: "Retrieved [\(key)] with value [\(value)]", level: .Authorisation)
        }
        
        if let token = responseDict["access_token"].string {
            accessToken = token
        }
        else { accessToken = nil }
        
        if let type = responseDict["token_type"].string {
            tokenType = type
        }
        else { tokenType = nil }
        
        if let expiry = responseDict["expires"].int {
            expires = expiry
        }
        else { expires = nil }
        
        if let timeTil = responseDict["expires_in"].int {
            expiresIn = timeTil
        }
        else { expiresIn = nil }
        
        if let refresh = responseDict["refresh_token"].string {
            refreshToken = refresh
        }
        else { refreshToken = nil }
    }
    
    func values() -> (access_token: String?, token_type: String?, expires_in: Int?, expires: Int?, refresh_token: String?) {
        return (accessToken, tokenType, expiresIn, expires, refreshToken)
    }
}

extension AniListAuthResponse: CustomStringConvertible {
    public var description: String {
        var string = "["
        
        if accessToken != nil {
            string.appendContentsOf("<Token>")
        }
        
        if tokenType != nil {
            string.appendContentsOf("<Token Type>")
        }
        
        if expires != nil {
            string.appendContentsOf("<\(expires)>")
        }
        
        if expiresIn != nil {
            string.appendContentsOf("<\(expiresIn)>")
        }
        
        if refreshToken != nil {
            string.appendContentsOf("<Refresh Token>")
        }
        
        string.appendContentsOf("]")
        
        return string
    }
    
    func isValid() -> Bool {
        return accessToken != nil && tokenType != nil && expires != nil && expiresIn != nil
    }
    
    func save() {
        let userDefaults = NSUserDefaults.standardUserDefaults()
        
        userDefaults.setObject(accessToken, forKey: ACCESS_TOKEN_KEY)
        userDefaults.setObject(tokenType, forKey: TOKEN_TYPE_KEY)
        userDefaults.setObject(expires, forKey: EXPIRES_KEY)
        userDefaults.setObject(expiresIn, forKey: EXPIRES_IN_KEY)
        userDefaults.setObject(refreshToken, forKey: REFRESH_TOKEN_KEY)
    }
    
    mutating func load() {
        let userDefaults = NSUserDefaults.standardUserDefaults()
        
        if let token = userDefaults.objectForKey(ACCESS_TOKEN_KEY) as? String {
            accessToken = token
        }
        
        if let type = userDefaults.objectForKey(TOKEN_TYPE_KEY) as? String {
            tokenType = type
        }
        
        if let expiry = userDefaults.objectForKey(EXPIRES_KEY) as? Int {
            expires = expiry
        }
        
        if let expiryIn = userDefaults.objectForKey(EXPIRES_IN_KEY) as? Int {
            expiresIn = expiryIn
        }
        
        if let refresh = userDefaults.objectForKey(REFRESH_TOKEN_KEY) as? String {
            refreshToken = refresh
        }
    }
    
    func clear() {
        let userDefaults = NSUserDefaults.standardUserDefaults()
        
        userDefaults.removeObjectForKey(ACCESS_TOKEN_KEY)
        userDefaults.removeObjectForKey(TOKEN_TYPE_KEY)
        userDefaults.removeObjectForKey(EXPIRES_KEY)
        userDefaults.removeObjectForKey(EXPIRES_IN_KEY)
        userDefaults.removeObjectForKey(REFRESH_TOKEN_KEY)
    }
}