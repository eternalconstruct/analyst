//
//  AniList.swift
//  Analyst
//
//  Created by Nick McVroom-Amoakohene on 17/05/2016.
//  Copyright © 2016 inslayn interactive. All rights reserved.
//

import Siesta
import SwiftyJSON

let AniList = AniListService()

class AniListService: Service {
    /* 
        NOTE: the static var is here instead of an extension because it causes a [A declaration cannot be both 'final' and 'dynamic'] compile error.
        This could be solved by annotating it with the @nonobjc tag, but its not critical that it needs to be in an extension, so I will leave it here.
     */
    static var onError: (title: String, message: String) -> () = { title, message in
        log(message: "<\(title)> \(message)", level: .Error)
    }
    
    private init() {
        super.init(baseURL: AnalystService.config?.apiUrl)
        
        log(message: "AniList service was initialised!", level: .Initialisation)
        
        configureSiesta()
        
        checkForExistingTokens()
    }
    
    private(set) var authResponse : AniListAuthResponse? {
        didSet {
            // Clear any cached data now that token has changed
            wipeResources()
            log(message: "Token has changed; cached data is now cleared.", level: .Authorisation)
            
            // Force resources to recompute headers next time they’re fetched
            invalidateConfiguration()
            log(message: "Resources will recompute their headers next time they are fetched.", level: .Authorisation)
            
            if let auth = authResponse {
                if let expires = auth.expires {
                    tokenExpiry = NSDate(timeIntervalSince1970: NSTimeInterval(expires))
                }
            }
        }
    }
    
    internal private(set) var tokenRefreshTime: NSDate? = nil
    
    internal private(set) var tokenExpiry: NSDate? {
        didSet {
            guard let tokenExpiry = tokenExpiry else { return }
            
            let timeToExpire = tokenExpiry.timeIntervalSinceNow
            
            if (timeToExpire < 0) {
                log(message: "Token has expired! Attempting to refresh it.", level: .Authorisation)
                attemptTokenRefresh()
            }
            else {
                tokenRefreshTime = NSDate(timeInterval: timeToExpire * REFRESH_THRESHOLD, sinceDate: NSDate())
                
                log(message: "Automatic token refresh is scheduled in \(tokenRefreshTime!.timeUntilDate)", level: .Authorisation)
                
                NSTimer.after(tokenRefreshTime!.timeIntervalSinceNow) {
                    self.attemptTokenRefresh()
                }
            }
        }
    }
    
    var contentType: String?
    
    private var accessToken: Resource { return resource("auth/access_token") }
    
    private var authorize: Resource { return resource("auth/authorize") }
    
    func checkForExistingTokens() {
        if var authresponse = AniListAuthResponse() {
            authresponse.load()
            
            if authresponse.isValid() {
                log(message: "Existing tokens found.", level: .Authorisation)
                authResponse = authresponse
                contentType = ContentType.WWW_FORM_URL_ENCODED
                return
            }
            
            log(message: "No existing tokens were found!", level: .Authorisation)
        }
    }
    
    func attemptTokenRefresh() {
        log(message: "Attempting automatic token refresh...", level: .Authorisation)
        
        guard (self.authResponse?.refreshToken) != nil else {
            log(message: "No refresh token was found! Authenticating as guest.", level: .Authorisation)
            do {
                try self.authenticate()
            }
            catch {
                log(message: "Couldn't automatically authenticate as guest.", level: .Authorisation)
            }
            
            return;
        }
        
        do {
            try self.authenticate(with: .Refresh)
        }
        catch {
            log(message: "Couldn't automatically authenticate using refresh token.", level: .Authorisation)
        }
    }
    
    func canAuthenticate() -> Bool {
        if let refreshTime = tokenRefreshTime {
            return refreshTime.isBeforeNow
        }
        
        return true
    }
    
    func authenticate(with grantType: AniListGrantType = .ClientCredentials, code: String? = nil) throws {
        log(message: "Grant type is [\(grantType.rawValue)]", level: .Authorisation)
        
        if canAuthenticate() {
            try requestAccessToken(with: grantType, code: code)
        }
        else {
            if let expiry = tokenExpiry {
                log(message: "Current access token has not expired. It will expire in \(expiry.timeUntilDate).", level: .General)
            }
        }
    }
    
    func authorise(with grantType: AniListGrantType = .Code)  throws {
        if let config = AnalystService.config {
            var parameters: [String : String] = [:]
            
            parameters["grant_type"] = grantType.rawValue
            parameters["client_id"] = config.clientId
            
            switch grantType {
            case .Code:
                parameters["redirect_uri"] = config.redirectUrl
                parameters["response_type"] = "code"
                break
            case .PIN:
                parameters["response_type"] = "pin"
                break
            default:
                log(message: "Grant type [\(grantType)] is not supported. Authorisation failed.", level: .Error)
                throw AuthorisationError.GrantType(message: "Grant type [\(grantType)] is not supported. Authorisation failed.")
            }
            
            var auth: Resource = authorize
            
            for parameter in parameters {
                auth = auth.withParam(parameter.0, parameter.1)
            }
            
            UIApplication.sharedApplication().openURL(auth.url)
        }
    }
    
    func requestAccessToken(with grantType: AniListGrantType, code: String?) throws {
        if let config = AnalystService.config {
            var parameters: [String : String] = [:]
            
            parameters["grant_type"] = grantType.rawValue
            parameters["client_id"] = config.clientId
            parameters["client_secret"] = config.clientSecret
            
            switch grantType {
            case .Code:
                parameters["redirect_uri"] = config.redirectUrl
                parameters["code"] = code
                break
            case .PIN:
                parameters["code"] = code
                break
            case .Refresh:
                if let auth = authResponse {
                    parameters["refresh_token"] = auth.refreshToken
                }
                break
            case .ClientCredentials:
                break
            default:
                log(message: "Grant type [\(grantType)] is not supported. Authentication failed.", level: .Error)
                throw AuthorisationError.GrantType(message: "Grant type [\(grantType)] is not supported. Authentication failed.")
            }
            
            _ = accessToken.request(.POST, json: parameters as NSDictionary)
                .onSuccess({ entity in
                    self.authenticationWasSuccessful(withResponse: entity.content as! AniListAuthResponse, andContentType: ContentType.WWW_FORM_URL_ENCODED)
                })
                .onFailure({ error in
                    self.authenticationHasFailed(error)
                })
        }
    }
    
    func clearTokens() {
        authResponse?.clear()
        authResponse = nil
        tokenExpiry = nil
        tokenRefreshTime = NSDate()
        log(message: "Authorisation token has been cleared.", level: .Authorisation)
    }
    
    func authenticationWasSuccessful(withResponse response: AniListAuthResponse, andContentType content: String) {
        response.save()
        
        authResponse = response
        contentType = content
        
        log(message: "Authentication was successful with response \(response) and content type \(content)", level: .Authorisation)
    }
    
    func authenticationHasFailed(error: Error) {
        log(message: "\(error.userMessage). Authentication has failed.", level: .Error)
    }
    
    func authorisationWasSuccessful() {
        log(message: "Authorisation was successful.", level: .Authorisation)
    }
    
    func authorisationHasFailed(error: Error) {
        log(message: "\(error.userMessage). Authorisation has failed.", level: .Error)
    }
}

extension AniListService {
    func series(seriesType: Series.SeriesType) -> Resource {
        return resource("\(seriesType.rawValue)")
    }
    
    func browse(seriesType: Series.SeriesType) -> Resource {
        return resource("browse/\(seriesType.rawValue)")
    }
    
    func toggleFavourite(seriesType: Series.SeriesType) -> Resource {
        return resource("\(seriesType.rawValue)/favourite")
    }
    
    var genreList: Resource { return resource("genre_list") }
    
    var currentlyAiringAnime: Resource { return browse(.Anime).withParam("status", Series.AiringStatus.Airing.rawValue) }
    
    var currentlyPublishingManga: Resource { return browse(.Manga).withParam("status", Series.PublishingStatus.Publishing.rawValue) }
}

extension AniListService {
    struct AniListErrorMessageExtractor: ResponseTransformer {
        func process(response: Response) -> Response {
            switch response {
            case .Success:
                return response
                
            case .Failure(var error):
                error.userMessage =
                    error.jsonDict["flash"] as? String ?? error.userMessage
                return .Failure(error)
            }
        }
    }
    
    func configureSiesta() {
        let swiftyJSONTransformer = ResponseContentTransformer(skipWhenEntityMatchesOutputType: false, transformErrors: false) {
            JSON($0.content as AnyObject)
        }
        
        configure {
            if let token = self.authResponse?.accessToken,
                let type = self.authResponse?.tokenType
            {
                $0.config.headers["Authorization"] = "\(type.uppercaseFirst) \(token)"
            }
            
            $0.config.headers["Content-Type"] = self.contentType
            $0.config.pipeline[.parsing].add(swiftyJSONTransformer, contentTypes: ["*/json"])
            $0.config.pipeline[.cleanup].add(AniListErrorMessageExtractor())
            
            if let config = AnalystService.config {
                $0.config.expirationTime = config.refreshInterval
            }
        }
        
        let authenticateUrl = accessToken.url
        let authoriseUrl    = authorize.url
        
        configure(whenURLMatches: { url in url != authenticateUrl && url != authoriseUrl }, description: "catch all authentication failures") {
            $0.config.beforeStartingRequest { resource, request in
                request.onFailure { error in
                    if error.httpStatusCode == 401 {
                        AniListService.onError(title: "Authentication Error", message: error.userMessage)
                    }
                }
            }
        }
    
        configureContentTransformers()
    }
    
    func configureContentTransformers() {
        configureTransformer("auth/access_token") {
            AniListAuthResponse(json: $0.content)
        }
        
        configureTransformer("\(Series.SeriesType.Anime.rawValue)/*") {
            Series(json: $0.content)
        }
        
        configureTransformer("browse/\(Series.SeriesType.Anime.rawValue)") {
            Series.list($0.content)
        }
        
        configureTransformer("\(Series.SeriesType.Manga.rawValue)/*") {
            Series(json: $0.content)
        }
        
        configureTransformer("browse/\(Series.SeriesType.Manga.rawValue)") {
            Series.list($0.content)
        }
    }
}

extension TypedContentAccessors {
    var seriesList: [Series] { return typedContent(ifNone: []) }
}
