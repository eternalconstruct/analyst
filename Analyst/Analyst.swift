//
//  Analyst.swift
//  Analyst
//
//  Created by Nick McVroom-Amoakohene on 15/05/2016.
//  Copyright © 2016 inslayn interactive. All rights reserved.
//

import Foundation
import Siesta

public let AnalystService = Analyst()

public class Analyst {
    public var onError:(title: String, message: String) -> () = { title, message in
        log(message: "<\(title)> \(message)", level: .Error)
    } { didSet { AniListService.onError = onError } }
    
    /**
     Represents the Japanese TV Seasons
     */
    public enum Season: String {
        /**
         No season, error state.
         */
        case None = "none"
        
        /**
         Winter TV Season (January – March)
         */
        case Winter = "winter"
        
        /**
         Spring TV Season (April – June)
         */
        case Spring = "spring"
        
        /**
         Summer TV Season (July – September)
         */
        case Summer = "summer"
        
        /**
         Fall TV Season (October – December)
         */
        case Fall   = "fall"
        
        /**
         Autumn TV Season (October – December)
         - Note: For my fellow Brits ;)
         */
        public static let Autumn = Season.Fall
        
        private static let allSeasons = [Spring, Summer, Fall, Winter]
        
        public static func otherSeasons(season: Season) -> [Season] {
            return allSeasons.filter { $0 != season }
        }
        
        public static func nextSeason(season: Season) -> Season {
            switch season {
            case .Winter:       // (January – March)
                return .Spring
            case .Spring:       // (April – June)
                return .Summer
            case .Summer:       // (July – September)
                return .Fall
            case .Fall:         // (October – December)
                return .Winter
            default:
                return .None
            }
        }
        
        public static func fromDate(date: NSDate) -> Season {
            let month = NSCalendar.currentCalendar().component(.Month, fromDate: date)
            
            switch month {
            case 1, 2, 3:       // (January – March)
                return .Winter
            case 4, 5, 6:       // (April – June)
                return .Spring
            case 7, 8, 9:       // (July – September)
                return .Summer
            case 10, 11, 12:    // (October – December)
                return .Fall
            default:
                return .None
            }
        }
    }
    
    private var observedResources: [Resource]
    
    internal private(set) var config: AnalystConfig?
    
    internal private(set) var initialised: Bool
    
    private init() {
        initialised = false
        observedResources = [Resource]()
        log(message: "Analyst instance created.", level: .Initialisation)
    }
    
    deinit {
        observedResources.removeAll()
        config = nil
        initialised = false
        log(message: "Goodbye.", level: .General)
    }
    
    /**
     Initialises Analyst.
     
     - parameter config: The configuration object to set Analyst up with.
     */
    public func initialise(withConfig config: AnalystConfig) throws {
        try checkCredentials(clientId: config.clientId, clientSecret: config.clientSecret, apiUrl: config.apiUrl)
        
        self.config = config
        
        log(message: "Client ID: \(config.clientId)", level: .Sensitive)
        log(message: "Client Secret: \(config.clientSecret)", level: .Sensitive)
        log(message: "API Url: \(config.apiUrl)", level: .Sensitive)
        log(message: "Redirect Url: \(config.redirectUrl)", level: .Sensitive)
        log(message: "Refresh Interval: \(config.refreshInterval)", level: .Sensitive)
        
        initialised = true
        log(message: "Initialisation complete.", level: .Initialisation)
    }
    
    /**
     Handles the application's openURL call.
     
     Note:
     should be called in the `application(app: UIApplication, openURL url: NSURL, options: [String : AnyObject]) -> Bool` function in the application's AppDelegate.
    */
    public func handleUrl(url: NSURL) {
        if let query = url.query {
            for param in query.componentsSeparatedByString("&") {
                var parts = param.componentsSeparatedByString("=")
                
                if parts.count == 2 && parts[0] == "code" {
                    let code = parts[1]
                    
                    do {
                        log(message: "Authorisation Code: \(code)", level: .Sensitive)
                        try AniList.authenticate(with: .Code, code: code)
                    }
                    catch {
                        log(message: "Couldn't automatically authenticate using the returned code.", level: .Authorisation)
                    }
                }
            }
        }
    }
    
    public func clearAuthentication() {
        AniList.clearTokens()
    }
    
    public func authenticateAsGuest() throws {
        do {
            try AniList.authenticate(with: .ClientCredentials)
        }
        catch {
            log(message: "Couldn't authenticate as guest.", level: .Authorisation)
        }
    }
    
    public func authenticateWithCode() throws {
        do {
            try AniList.authorise(with: .Code)
        }
        catch {
            log(message: "Couldn't authenticate with code.", level: .Authorisation)
        }
    }
    
    /**
     Authenticates using the PIN method.
     - Note: This method must be called twice. Once to request the PIN, and then once more to pass the PIN to the service to retrieve the access token.
     - e.g. First call `authenticateWithPIN();` followed by `authenticateWithPIN("returned_code");`
     */
    public func authenticateWithPIN(code code: String? = nil) throws {
        if let code = code {
            do {
                try AniList.authenticate(with: .PIN, code: code)
            }
            catch {
                log(message: "Couldn't automatically authenticate using the passed code.", level: .Authorisation)
            }
        }
        else {
            do {
                try AniList.authorise(with: .PIN)
            }
            catch {
                log(message: "Couldn't authenticate with PIN.", level: .Authorisation)
            }
        }
    }
    
    public func series(id: String, seriesType: Series.SeriesType, closure: (item: Series?) -> ()) {
        let seriesResource = AniList.series(seriesType).child(id)
        
        addObserver(forResource: seriesResource, observerLimit: DEFAULT_OBSERVER_LIMIT, owner: self) { resource, event in
            if let series: Series = resource.typedContent() {
                log(message: "\(seriesType.rawValue) series with id \(id) retrieved.", level: .General)
                
                return closure(item: series)
            }
            
            if let error = resource.latestError {
                self.cleanupObservers(forResource: seriesResource, ownedBy: self)
                log(message: error.userMessage, level: .Error)
            }
        }
        .loadIfNeeded()
    }
    
    public func currentlyAiringAnime(withAiringInfo: Bool = true, fetchAllResults: Bool = true, closure: (list: [Series]) -> ()) {
        let currentlyAiring = AniList.currentlyAiringAnime.withParam("full_page", fetchAllResults.description).withParam("airing_data", withAiringInfo.description)
                
        addObserver(forResource: currentlyAiring, observerLimit: DEFAULT_OBSERVER_LIMIT, owner: self) { resource, event in
            if resource.typedContent() != nil {
                log(message: "\(Mirror(reflecting: resource.typedContent()!).subjectType) is the type of the thing received", level: .General)
            }
            
            if let animeList: [Series] = resource.typedContent() {
                log(message: "\(animeList.count) currently airing anime retrieved.", level: .General)
                
                return closure(list: animeList)
            }
            
            if let error = resource.latestError {
                self.cleanupObservers(forResource: currentlyAiring, ownedBy: self)
                log(message: error.userMessage, level: .Error)
            }
        }
        .loadIfNeeded()
    }
    
    public func query(queryString: String, endpoint: String, withAiringInfo: Bool = true, closure: (list: [Series]) -> ()) {
        log(message: "Executing query [\(queryString)] on endpoint [\(endpoint)]", level: .General)
        
        var queryResource = AniList.resource(endpoint)
        
        for parameter in queryString.componentsSeparatedByString("&") {
            let kv = parameter.componentsSeparatedByString("=")
            
            queryResource = appendParameters(queryResource, parameterName: kv.first!, parameterValue: kv.last!)
        }
        
        query(queryString, endpoint: queryResource, withAiringInfo: withAiringInfo, closure: closure)
    }
    
    public func query(queryString: String, endpoint: Resource, withAiringInfo: Bool = true, closure: (list: [Series]) -> ()) {
        var resource = endpoint
        
        for parameter in queryString.componentsSeparatedByString("&") {
            let kv = parameter.componentsSeparatedByString("=")
            
            resource = appendParameters(resource, parameterName: kv.first!, parameterValue: kv.last!)
        }
        
        resource = appendParameters(resource, parameterName: "airing_data", parameterValue: withAiringInfo.description)
        
        log(message: "Executing query [\(queryString)] on endpoint [\(resource.description)]", level: .General)
        
        addObserver(forResource: resource, observerLimit: DEFAULT_OBSERVER_LIMIT, owner: self) { resource, event in
            if let animeList: [Series] = resource.typedContent() {
                log(message:
                    "\(animeList.count) anime retrieved using query [\(queryString)] on endpoint [\(endpoint)].",
                    level: .General)
                self.cleanupObservers(forResource: endpoint, ownedBy: self)
                return closure(list: animeList)
            }
            
            if let error = resource.latestError {
                self.cleanupObservers(forResource: endpoint, ownedBy: self)
                log(message: error.userMessage, level: .Error)
                self.onError(title: "Error", message: error.userMessage)
            }
        }
        .loadIfNeeded()
    }
    
    /**
     Returns a list of series whose genres match the series passed.
     Uses the top 2 genres by default
     
     - parameter series: The series to use as a base for suggestions.
     - parameter seriesType: The type of series.
     - parameter limit: Sets how many genres to retrieve suggestions for.
     */
    public func suggestions(series: Series, seriesType: Series.SeriesType, limit: Int = 2, closure: (list: [Series]) -> ()) {
        suggestions([series], seriesType: seriesType, limit: limit, closure: closure)
    }
    
    /**
     Returns a list of series whose genres match the list of anime passed.
     Uses the top 2 genres by default
     
     - parameter series: The list of anime to use as a base for suggestions.
     - parameter seriesType: The type of series.
     - parameter limit: Sets how many genres to retrieve suggestions for.
     */
    public func suggestions(series: [Series], seriesType: Series.SeriesType, limit: Int = 2, closure: (list: [Series]) -> ()) {
        
        var genreList = [String]()
        
        for item in series {
            assert(item.seriesType == seriesType, "series must be the same type as seriesType!") // TODO: Add fix instead of assert, and add tests
            
            if let genres = item.genres {
                genreList.appendContentsOf(genres)
            }
        }
        
        let genres = genreList.frequencies().sort({ $0.1 > $1.1 }).prefix(limit).map({ $0.0 })
        
        let genreString = genres.joinWithSeparator(",")
        
        let queryString = "genres=\(genreString)"
        
        log(message: "Retrieving anime that match the following genres: \(genreString)", level: .General)
        
        query(queryString, endpoint: AniList.browse(seriesType)) { suggestionList in
            return closure(list: suggestionList)
        }
    }
    
    public func animeAiringToday(closure: (list: [Series]) -> ()) {
        currentlyAiringAnime() { list in
            let hasAiringInfo = list.filter({ $0.airingStats != nil })
            
            closure(list: hasAiringInfo.filter({
                if let airing = $0.airingStats {
                    if let time = airing.time {
                        if let date = self.getDateFromTimeString(time) {
                            return date.isToday
                        }
                        
                        return false
                    }
                }
                
                return false
            }))
        }
    }
    
    public func animeAiring(onDate date: NSDate, closure: (list: [Series]) -> ()) {
        currentlyAiringAnime() { list in
            let hasAiringInfo = list.filter({ $0.airingStats != nil })
            
            closure(list: hasAiringInfo.filter({
                if let airing = $0.airingStats {
                    if let time = airing.time {
                        if let converted = self.getDateFromTimeString(time) {
                            return converted.isEqualToDateIgnoringTime(date)
                        }
                        
                        return false
                    }
                }
                
                return false
            }))
        }
    }
    
    public func seriesForSeason(season: Season, seriesType: Series.SeriesType, closure: (list: [Series]) -> ()) {
        query("season=\(season.rawValue)", endpoint: AniList.browse(seriesType)) { list in
            closure(list: list)
        }
    }
    
    public func seriesForUpcomingSeason(seriesType: Series.SeriesType, closure: (list: [Series]) -> ()) {
        let season = Season.fromDate(NSDate())
        
        let next: Season = Season.nextSeason(season)
        
        query("season=\(next.rawValue)", endpoint: AniList.browse(seriesType)) { list in
            closure(list: list)
        }
    }
    
    public func toggleFavouriteStatus(series: Series) {
        toggleFavouriteStatus(series.id, seriesType: series.seriesType)
    }
    
    func toggleFavouriteStatus(id: Int, seriesType: Series.SeriesType) {
        AniList.toggleFavourite(seriesType).request(.POST, json: ["id" : id])
            .onSuccess { entity in
                log(message: "Toggled favourite status for anime with id \(id).", level: LogLevel.General)
            }
            .onFailure { error in
                log(message: "Couldn't toggle favourite status for anime with id \(id).", level: LogLevel.Error)
        }
    }
    
    // MARK: Utility functions
    
    /**
     Adds a closure observer to a given resource.
     
     The resource holds a weak reference to `owner`, and the closure will receive events only as long as `owner`
     still exists.
     
     - Note: Setting the `observerLimit` will limit the amount of observers that can be registered to this resource.
     */
    func addObserver(forResource resource: Resource, observerLimit: Int, owner: AnyObject, closure: ResourceObserverClosure) -> Resource {
        if observedResources.filter({ $0 == resource }).count < observerLimit {
            log(message: "Adding a new observer to resource.", level: LogLevel.Resources)
            observedResources.append(resource)
            return resource.addObserver(owner: owner, closure: closure)
        }
        
        log(message: "Observer limit reached. Freeing previous observer.", level: LogLevel.Resources)
        cleanupObservers(forResource: resource, ownedBy: owner)
        return addObserver(forResource: resource, observerLimit: observerLimit, owner: owner, closure: closure)
    }
    
    func cleanupObservers(forResource resource: Resource, ownedBy owner: AnyObject?) {
        var observed = observedResources.filter({ $0 == resource })
        
        for i in 0 ..< observed.count {
            if let index = observedResources.indexOf(observed[i]) {
                log(message: "Removing observer for resource [\(resource)].", level: LogLevel.Resources)
                observedResources.removeAtIndex(index)
            }
        }
        
        resource.removeObservers(ownedBy: owner)
    }

    func appendParameters(resource: Resource, parameterName: String, parameterValue: String) -> Resource {
        // could have used inout for the resource param, but not returning the result of withParam() causes a warning
        return resource.withParam(parameterName, parameterValue)
    }
    
    func checkCredentials(clientId client_id: String, clientSecret secret: String, apiUrl url: String) throws {
        guard !client_id.isEmpty else { throw InitialisationError.ClientID(message: "Client ID is not set!") }
        guard !secret.isEmpty else { throw InitialisationError.ClientSecret(message: "Client Secret is not set!") }
        guard !url.isEmpty else { throw InitialisationError.APIUrl(message: "API Root URL is not set!") }
        
        // This will be thrown on a unauthorised response code Error(userMessage: "Unauthorized", httpStatusCode: Optional(401), entity: Optional(Siesta.Entity(content: [error: invalid_client, error_description: Client authentication failed]
        guard client_id != "wrong_client_id" && secret != "wrong_client_secret" else {
            throw InitialisationError.Failed(message: "Initialisation failed due to incorrect credentials!")
        }
        
        // This will be thrown on any other errors
        guard url != "wrong_api_url" else {
            throw InitialisationError.Failed(message: "Initialisation failed, probably an incorrect API Url!")
        }
    }
    
    func getDateFromTimeString(time: String) -> NSDate? {
        var date: NSDate? = nil
        
        do {
            date = try time.toDate()
        }
        catch String.DateConversionError.BadDate(let message) {
            log(message: message, level: .Error)
        }
        catch String.DateConversionError.DateNotFound(let message) {
            log(message: message, level: .Error)
        }
        catch {
            log(message: "An unknown date conversion error occurred.", level: .Error)
        }
        
        return date
    }
    
}