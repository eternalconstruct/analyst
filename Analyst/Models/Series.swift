//
//  Series.swift
//  Analyst
//
//  Created by Nick McVroom-Amoakohene on 26/05/2016.
//  Copyright © 2016 inslayn interactive. All rights reserved.
//

/*
 Changelog
 - 2016.08.22: 
    Changed from Anime to Series; Refactored several properties into struct containers and enums [Images, Status, Media, Source, etc.]
    This was done to conform to the new JSON sent from the AniList API.
 */

import SwiftyJSON

public struct Series {
    public private(set) var id:                 Int = 0
    public private(set) var seriesType:         SeriesType = .Unknown // series type will be set dynamically; if it is still unknown, something went wrong :)
    
    public private(set) var titles:             Title?
    
    public private(set) var mediaType:          Media?
    
    @available(*, deprecated=0.0.1)
    public private(set) var startDate:          String?
    
    @available(*, deprecated=0.0.1)
    public private(set) var endDate:            String?
    
    public private(set) var fuzzyStartDate:     Int?
    public private(set) var fuzzyEndDate:       Int?
    
    private             var _season:            Int?
    public              var season:             Season { get { return Season.Fall /* TODO: parse int to get the last digit to init Season with. */ } }
    public              var year:               Int { get { return 2016 /* TODO: parse int to get the first two digits to create year int. */ } }
    
    public private(set) var seriesDescription:  String?
    
    public private(set) var synonyms:           [String]?
    public private(set) var genres:             [String]? // Should be enum? check genre list
    
    
    public private(set) var isAdult:            Bool = false
    public private(set) var averageScore:       Double = 0 // 1 - 100
    
    public private(set) var popularity:         Int = 0 // Number of users with the series in their lists
    public private(set) var isFavourite:        Bool = false // True if current authenticated user has favourited this series; false otherwise or if not authenticated
    
    public private(set) var images:             Images?
    
    public private(set) var updatedAt:          Int = 0
    public private(set) var scoreDistribution:  [String]?
    public private(set) var listStats:          ListStats?
    
    // Anime-only
    
    public private(set) var totalEpisodes:      Int = 0
    public private(set) var duration:           Int?
    
    public private(set) var airingStatus:       AiringStatus?
    
    public private(set) var youtubeId:          String?
    public private(set) var hashtag:            String?
    
    public private(set) var source:             Source?
    
    public private(set) var airingStats:        AiringStats?
    
    // Manga-only
    
    public private(set) var totalChapters:      Int = 0
    public private(set) var totalVolumes:       Int = 0
    
    public private(set) var publishingStatus:   PublishingStatus?
}

extension Series {
    public enum SeriesType: String {
        case Anime      = "anime"
        case Manga      = "manga"
        case Unknown    = "unknown"
    }
    
    public enum Season: Int {
        /**
         Winter TV Season (January – March)
         */
        case Winter = 1
        
        /**
         Spring TV Season (April – June)
         */
        case Spring = 2
        
        /**
         Summer TV Season (July – September)
         */
        case Summer = 3
        
        /**
         Fall TV Season (October – December)
         */
        case Fall   = 4
        
        /**
         Autumn TV Season (October – December)
         - Note: For my fellow Brits ;)
         */
        public static let Autumn = Season.Fall
        
    }
    
    public enum Media: Int {
        case TV         = 0
        case TVShort    = 1
        case Movie      = 2
        case Special    = 3
        case OVA        = 4
        case ONA        = 5
        case Music      = 6
        case Manga      = 7
        case Novel      = 8
        case OneShot    = 9
        case Doujin     = 10
        case Manhua     = 11
        case Manhwa     = 12
    }
    
    public enum Source: String {
        case Original       = "Original"
        case Manga          = "Manga"
        case LightNovel     = "Light Novel"
        case VisualNovel    = "Visual Novel"
        case VideoGame      = "Video Game"
        case Other          = "Other"
    }
    
    public enum AiringStatus: String {
        case Finished   = "finished airing"
        case Airing     = "currently airing"
        case NotAired   = "not yet aired"
        case Cancelled  = "cancelled"
    }
    
    public enum PublishingStatus: String {
        case Finished       = "finished publishing"
        case Publishing     = "publishing"
        case NotPublished   = "not yet published"
        case Cancelled      = "cancelled"
    }
    
    public struct Title {
        public private(set) var Romaji:        String?
        public private(set) var Japanese:      String?
        public private(set) var English:       String?
        
        init(romaji: String?, japanese: String?, english: String?) {
            Romaji     = romaji
            Japanese   = japanese
            English    = english
        }
    }
    
    public struct Images {
        public private(set) var Small:      String? // 24x39 (not available for manga)
        public private(set) var Medium:     String? // 93x133
        public private(set) var Large:      String? // 225x323
        public private(set) var Banner:     String? // 1720x390
        
        init(small: String?, medium: String?, large: String?, banner: String?) {
            Small   = small
            Medium  = medium
            Large   = large
            Banner  = banner
        }
    }
}

extension Series {
    init?(json: JSON) {
        if (json.isEmpty) {
            return nil
        }
        
        setCommonProperties(json)
        
        setAdditionalProperties(json)
        
        switch seriesType {
        case .Anime:
            setAnimeProperties(json)
        case .Manga:
            setMangaProperties(json)
        default:
            log(message: "This series with id \(id) had an unknown series type! Something when wrong.", level: .Error)
            return nil
        }
    }
    
    static func list(fromJSON: JSON) -> [Series] {
        var series = [Series]()
        
        if let arr = fromJSON.array {
            for items in arr {
                if let item = Series(json: items) {
                    series.append(item)
                }
            }
        }
        else if let dict = fromJSON.dictionary {
            if let item = Series(json: JSON(dict)) {
                series.append(item)
            }
        }
        
        return series
    }
    
    mutating func setCommonProperties(json: JSON) {
        id                  = json["id"].intValue
        seriesType          = SeriesType(rawValue: json["series_type"].stringValue) ?? SeriesType.Unknown
        titles              = Title(romaji: json["title_romaji"].stringValue, japanese: json["title_japanese"].stringValue, english: json["title_english"].stringValue)
        mediaType           = Media(rawValue: json["type"].intValue)
        startDate           = json["start_date"].string ?? ""   // FIXME: This property is deprecated and should be removed
        endDate             = json["end_date"].string ?? ""     // FIXME: This property is deprecated and should be removed
        fuzzyStartDate      = json["start_date_fuzzy"].int ?? nil
        fuzzyEndDate        = json["end_date_fuzzy"].int ?? nil
        _season             = json["season"].int ?? nil
        seriesDescription   = json["description"].stringValue
        synonyms            = json["synonyms"].arrayObject as? [String] ?? []
        genres              = json["genres"].arrayObject as? [String] ?? []
    }
    
    mutating func setAdditionalProperties(json: JSON) {
        isAdult             = json["adult"].boolValue
        averageScore        = json["average_score"].doubleValue
        popularity          = json["popularity"].intValue
        isFavourite         = json["favourite"].boolValue
        images              = Images(small: json["image_url_sml"].stringValue,
                                     medium: json["image_url_med"].stringValue,
                                     large: json["image_url_lge"].stringValue,
                                     banner: json["image_url_banner"].stringValue)
        updatedAt           = json["updated_at"].intValue
        scoreDistribution   = json["score_distribution"].arrayObject as? [String] ?? []
        listStats           = ListStats(dict: json["list_stats"].dictionaryObject ?? [:])
    }
    
    mutating func setAnimeProperties(json: JSON) {
        totalEpisodes   = json["total_episodes"].intValue
        duration        = json["duration"].int ?? nil
        airingStatus    = AiringStatus(rawValue: json["airing_status"].stringValue)
        youtubeId       = json["youtube_id"].stringValue
        hashtag         = json["hashtag"].stringValue
        source          = Source(rawValue: json["source"].stringValue)
        airingStats     = AiringStats(dict: json["airing_stats"].dictionaryObject ?? [:])
    }
    
    mutating func setMangaProperties(json: JSON) {
        totalChapters       = json["total_chapters"].intValue
        totalVolumes        =  json["total_volumes"].intValue
        publishingStatus    = PublishingStatus(rawValue: json["publishing_status"].stringValue)
    }
}