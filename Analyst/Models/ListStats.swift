//
//  ListStats.swift
//  Analyst
//
//  Created by Nick McVroom-Amoakohene on 18/08/2016.
//  Copyright © 2016 inslayn interactive. All rights reserved.
//

import Foundation

public struct ListStats {
    public private(set) var planToWatch:    Int?
    public private(set) var watching:       Int?
    public private(set) var completed:      Int?
    public private(set) var onHold:         Int?
    public private(set) var dropped:        Int?
}

extension ListStats {
    init?(dict: [String : AnyObject?]) {
        if (dict.isEmpty) {
            return nil
        }
        
        (planToWatch, watching, completed, onHold, dropped) = (dict["plan_to_watch"] as? Int ?? 0, dict["watching"] as? Int  ?? 0, dict["completed"] as? Int  ?? 0, dict["on_hold"] as? Int  ?? 0, dict["dropped"] as? Int  ?? 0)
    }
}