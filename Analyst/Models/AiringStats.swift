//
//  AiringStats.swift
//  Analyst
//
//  Created by Nick McVroom-Amoakohene on 18/08/2016.
//  Copyright © 2016 inslayn interactive. All rights reserved.
//

import Foundation

public struct AiringStats {
    public private(set) var time:           String?
    public private(set) var countdown:      Int?
    public private(set) var nextEpisode:    Int?
    
}

extension AiringStats {
    init?(dict: [String : AnyObject?]) {
        if (dict.isEmpty) {
            return nil
        }
        
        (time, countdown, nextEpisode) = (dict["time"] as? String ?? "", dict["countdown"] as? Int ?? 0, dict["next_episode"] as? Int ?? 0)
    }
}