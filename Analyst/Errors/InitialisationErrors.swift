//
//  InitialisationErrors.swift
//  Analyst
//
//  Created by Nick McVroom-Amoakohene on 15/05/2016.
//  Copyright © 2016 inslayn interactive. All rights reserved.
//

public enum InitialisationError: ErrorType {
    case ClientID(message: String)
    case ClientSecret(message: String)
    case APIUrl(message: String)
    case Failed(message: String)
}