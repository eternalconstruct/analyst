//
//  AuthorisationErrors.swift
//  Analyst
//
//  Created by Nick McVroom-Amoakohene on 15/05/2016.
//  Copyright © 2016 inslayn interactive. All rights reserved.
//

import Foundation

public enum AuthorisationError: ErrorType {
    case AccessToken(message: String)
    case GrantType(message: String)
}