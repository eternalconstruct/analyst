//
//  AnalystErrors.swift
//  Analyst
//
//  Created by Nick McVroom-Amoakohene on 17/05/2016.
//  Copyright © 2016 inslayn interactive. All rights reserved.
//

public enum AnalystError: ErrorType {
    case Query(message: String)
}
