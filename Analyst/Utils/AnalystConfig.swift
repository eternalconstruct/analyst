//
//  Helpers.swift
//  Analyst
//
//  Created by Nick McVroom-Amoakohene on 16/05/2016.
//  Copyright © 2016 inslayn interactive. All rights reserved.
//

import Foundation
import Siesta
import SwiftyJSON

public class AnalystConfig {
    public private(set) var clientId: String
    public private(set) var clientSecret: String
    public private(set) var apiUrl: String
    
    /**
     Sets the redirect url for use with authorisation methods requiring callbacks.
     */
    public var redirectUrl: String?
    
    /**
     How long (in seconds) should Analyst wait to consider data as stale (out-of-date)?
     */
    public var refreshInterval: NSTimeInterval = 30 {
        didSet {
            if refreshInterval < 0 {
                refreshInterval = 30 // Siesta default
            }
            
            log(message: "Data will be considered stale after \(refreshInterval) seconds.", level: .Configuration)
        }
    }
    
    /**
     Enables/disables debug messages.
    */
    public var debug: Bool = false {
        didSet {
            if debug {
                log(message: "All logging enabled.", level: .Configuration, force: true)
                
                // Turn on Analyst debugging
                enabledLogLevel = LogLevel.all
                
                // Turn on Siesta debugging
                enabledLogCategories = LogCategory.all
            }
            else {
                log(message: "All logging disabled.", level: .Configuration, force: true)
                
                // Turn off Analyst debugging
                enabledLogLevel = LogLevel.none
                
                // Turn off Siesta debugging
                enabledLogCategories = Set<LogCategory>()
            }
        }
    }
    
    /**
     Analyst configuration object.
     
    - parameter id:        The client ID found in the application settings. This identifies the client to the server.
    - parameter secret:    The client secret found in the application settings
    - parameter url:       The API url, for example, https://anilist.co/api/
    */
    public init(clientId id: String, clientSecret secret: String, apiUrl url: String) {
        (clientId, clientSecret, apiUrl) = (id, secret, url)
    }
}