//
//  TimeIntervalExtensions.swift
//  Analyst
//
//  Created by Nick McVroom-Amoakohene on 18/08/2016.
//  Copyright © 2016 inslayn interactive. All rights reserved.
//

import Foundation

extension NSTimeInterval {
    /**
     Returns an NSDate representing the time interval since 1970.
     */
    public var asDate: NSDate { return NSDate(timeIntervalSince1970: abs(self)) }
}