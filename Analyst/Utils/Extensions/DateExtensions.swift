//
//  DateExtensions.swift
//  Analyst
//
//  Created by Nick McVroom-Amoakohene on 26/05/2016.
//  Copyright © 2016 inslayn interactive. All rights reserved.
//

import Foundation

extension NSDate: Comparable {
    public var isBeforeNow: Bool { return self < NSDate() }
    public var isAfterNow: Bool { return self > NSDate() }
    
    public var isToday: Bool {
        return isEqualToDateIgnoringTime(NSDate())
    }
    
    public func isEqualToDateIgnoringTime(date: NSDate) -> Bool {
        let calendar = NSCalendar.currentCalendar()
        
        let ay = calendar.dateFromComponents(calendar.components([.Era, .Year, .Month, .Day], fromDate: date))
        let be = calendar.dateFromComponents(calendar.components([.Era, .Year, .Month, .Day], fromDate: self))
        
        return ay!.isEqualToDate(be!)
    }
    
    public var timeUntilDate: String { return self.offsetFrom(NSDate()) }
    public var timeSinceDate: String { return NSDate().offsetFrom(self) }
    
    public func yearsFrom(date: NSDate) -> Int {
        return NSCalendar.currentCalendar().components(.Year, fromDate: date, toDate: self, options: []).year
    }
    
    public func monthsFrom(date: NSDate) -> Int {
        return NSCalendar.currentCalendar().components(.Month, fromDate: date, toDate: self, options: []).month
    }
    
    public func weeksFrom(date: NSDate) -> Int {
        return NSCalendar.currentCalendar().components(.WeekOfYear, fromDate: date, toDate: self, options: []).weekOfYear
    }
    
    public func daysFrom(date: NSDate) -> Int {
        return NSCalendar.currentCalendar().components(.Day, fromDate: date, toDate: self, options: []).day
    }
    
    public func hoursFrom(date: NSDate) -> Int {
        return NSCalendar.currentCalendar().components(.Hour, fromDate: date, toDate: self, options: []).hour
    }
    
    public func minutesFrom(date: NSDate) -> Int {
        return NSCalendar.currentCalendar().components(.Minute, fromDate: date, toDate: self, options: []).minute
    }
    
    public func secondsFrom(date: NSDate) -> Int {
        return NSCalendar.currentCalendar().components(.Second, fromDate: date, toDate: self, options: []).second
    }
    
    public func offsetFrom(date: NSDate) -> String {
        if yearsFrom(date)   > 0 { return "\(yearsFrom(date))y"   }
        if monthsFrom(date)  > 0 { return "\(monthsFrom(date))M"  }
        if weeksFrom(date)   > 0 { return "\(weeksFrom(date))w"   }
        if daysFrom(date)    > 0 { return "\(daysFrom(date))d"    }
        if hoursFrom(date)   > 0 { return "\(hoursFrom(date))h"   }
        if minutesFrom(date) > 0 { return "\(minutesFrom(date))m" }
        if secondsFrom(date) > 0 { return "\(secondsFrom(date))s" }
        return ""
    }
}

public func +(lhs: NSDate, rhs: NSTimeInterval) -> NSDate {
    return lhs.dateByAddingTimeInterval(rhs)
}

public func -(lhs: NSDate, rhs: NSTimeInterval) -> NSDate {
    return lhs.dateByAddingTimeInterval(-rhs)
}

public func ==(lhs: NSDate, rhs: NSDate) -> Bool {
    return lhs === rhs || lhs.compare(rhs) == .OrderedSame
}

public func !=(lhs: NSDate, rhs: NSDate) -> Bool {
    return lhs !== rhs || lhs.compare(rhs) != .OrderedSame
}

public func <(lhs: NSDate, rhs: NSDate) -> Bool {
    return lhs.compare(rhs) == .OrderedAscending
}

public func >(lhs: NSDate, rhs: NSDate) -> Bool {
    return lhs.compare(rhs) == .OrderedDescending
}

public func <=(lhs: NSDate, rhs: NSDate) -> Bool {
    return lhs.compare(rhs) == .OrderedAscending || lhs.compare(rhs) == .OrderedSame
}

public func >=(lhs: NSDate, rhs: NSDate) -> Bool {
    return lhs.compare(rhs) == .OrderedDescending || lhs.compare(rhs) == .OrderedSame
}