//
//  StringExtensions.swift
//  Analyst
//
//  Created by Nick McVroom-Amoakohene on 26/05/2016.
//  Copyright © 2016 inslayn interactive. All rights reserved.
//

import Foundation

extension String {
    public enum DateConversionError: ErrorType {
        case BadDate(message: String)
        case DateNotFound(message: String)
    }
    
    public var first: String { return String(characters.prefix(1)) }
    public var last: String { return String(characters.suffix(1)) }
    public var uppercaseFirst: String { return first.uppercaseString + String(characters.dropFirst()) }
    
    public func toDate() throws -> NSDate {
        let dateFormatter = NSDateFormatter()
        // 2016-08-19T22:30:00+09:00
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZZZZ"
        
        if self.isEmpty { throw DateConversionError.DateNotFound(message: "Cannot convert an empty string!") }
        
        guard let date = dateFormatter.dateFromString(self) else
        {
            throw DateConversionError.BadDate(message: "The given string, \(self), is in an incorrect format. It must match the format \(dateFormatter.dateFormat).")
        }
        
        return date
    }
}
