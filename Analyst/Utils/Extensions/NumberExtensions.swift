//
//  NumberExtensions.swift
//  Analyst
//
//  Created by Nick McVroom-Amoakohene on 18/08/2016.
//  Copyright © 2016 inslayn interactive. All rights reserved.
//

import Foundation

extension Int {
    /**
     Converts itself to a time interval, and returns an NSDate representing the time interval since 1970. Ignores negatives.
     */
    public var asDate: NSDate { return NSDate(timeIntervalSince1970: NSTimeInterval(abs(self))) }
    
    public var secondsToDaysHoursMinutesSeconds: (Int, Int, Int, Int) {
        let secondsInAMinute: Float = 60.0
        let secondsInAnHour: Float  = 60.0 * secondsInAMinute
        let secondsInADay: Float    = 24.0 * secondsInAnHour
        
        // extract days
        let days = floor(Float(abs(self)) / secondsInADay)
        
        // extract hours
        let hourSeconds = Float(abs(self)) % secondsInADay;
        let hours = floor(hourSeconds / secondsInAnHour);
        
        // extract minutes
        let minuteSeconds = hourSeconds % secondsInAnHour;
        let minutes = floor(minuteSeconds / secondsInAMinute);
        
        // extract the remaining seconds
        let remainingSeconds = minuteSeconds % secondsInAMinute;
        let seconds = ceil(remainingSeconds)
        
        return (Int(days), Int(hours), Int(minutes), Int(seconds))
    }
}
