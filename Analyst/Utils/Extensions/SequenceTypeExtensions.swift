//
//  SequenceTypeExtensions.swift
//  Analyst
//
//  Created by Nick McVroom-Amoakohene on 18/08/2016.
//  Copyright © 2016 inslayn interactive. All rights reserved.
//

import Foundation

extension SequenceType where Generator.Element: Hashable {
    public func frequencies() -> [(Generator.Element,Int)] {
        
        var frequency: [Generator.Element:Int] = [:]
        
        for x in self {
            frequency[x] = (frequency[x] ?? 0) + 1
        }
        
        return frequency.sort { $0.1 > $1.1 }
    }
}