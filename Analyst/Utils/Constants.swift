//
//  Constants.swift
//  Analyst
//
//  Created by Nick McVroom-Amoakohene on 19/05/2016.
//  Copyright © 2016 inslayn interactive. All rights reserved.
//

import Foundation

public let Name: String = "Analyst"

let DEFAULT_OBSERVER_LIMIT: Int = 1
let REFRESH_THRESHOLD: NSTimeInterval = 0.9 // How much of the time remaining to refresh should pass before triggering a refresh

struct ContentType {
    static let WWW_FORM_URL_ENCODED: String = "application/x-www-form-urlencoded"
    static let JSON: String = "application/json"
}