//
//  Logger.swift
//  Analyst
//
//  Created by Nick McVroom-Amoakohene on 18/05/2016.
//  Copyright © 2016 inslayn interactive. All rights reserved.
//

import Foundation

// Based on Siesta's logging (so awesome)

public enum LogLevel: String
{
    case Authorisation
    
    case Configuration

    case Error
    
    case General
    
    case Initialisation
    
    case Sensitive
    
    case Cache
    
    case Resources
    
    // MARK: Predefined subsets
    
    public static let `default`: Set<LogLevel> = [Error, General]
    public static let all: Set<LogLevel> = [Authorisation, Configuration, Error, General, Initialisation, Sensitive, Cache, Resources]
    public static let nonsensitive = Set<LogLevel>(all.filter { $0 != Sensitive })
    public static let none: Set<LogLevel> = Set<LogLevel>()
}

var logger: (String, LogLevel) -> Void = { print("[\(Name):\($1.rawValue)] \($0)") }

/**
 Sets the logging level. 
*/
public var enabledLogLevel = LogLevel.none

internal func log(message text: String, level: LogLevel, force: Bool = false) {
    if (enabledLogLevel.contains(level) || force) {
        logger(text, level)
    }
}