//
//  AnalystAiringInfoTests.swift
//  Analyst
//
//  Created by Nick McVroom-Amoakohene on 20/08/2016.
//  Copyright © 2016 inslayn interactive. All rights reserved.
//

import Quick
import Nimble

@testable import Analyst

class AnalystAiringInfoTests: QuickSpec {
    let data: [String : AnyObject?] = ["time" : "2001-09-09T01:46:40+0900", "countdown" : 190289893, "next_episode" : 9001]
    let garbage: [String : AnyObject?] = ["tim" : "2001-09-09T01:46:40+0900", "contdown" : 190289893, "nxt_episode" : 9001]
    
    override func spec() {
        // FIXME: This spec is disabled because for some reason it causes Mockingjay to not be able to stub requests.
        xdescribe("Analyst Airing Info Model Tests") {
            context("if data is empty") {
                it("should return nil") {
                    expect(AiringInfo(dict: [:])).to(beNil())
                }
            }
            
            context("if data is valid") {
                it("should not return nil") {
                    expect(AiringInfo(dict: self.data)).toNot(beNil())
                }
                
                it("should have a value for time") {
                    expect(AiringInfo(dict: self.data)?.time).toNot(beNil())
                    expect(AiringInfo(dict: self.data)?.time).to(equal("2001-09-09T01:46:40+0900"))
                }
                
                it("should have a value for countdown") {
                    expect(AiringInfo(dict: self.data)?.countdown).toNot(beNil())
                    expect(AiringInfo(dict: self.data)?.countdown).to(beGreaterThanOrEqualTo(0))
                }
                
                it("should have a value for next episode") {
                    expect(AiringInfo(dict: self.data)?.nextEpisode).toNot(beNil())
                    expect(AiringInfo(dict: self.data)?.nextEpisode).to(beGreaterThanOrEqualTo(0))
                }
            }
            
            context("if data is not valid") {
                it("should not return nil") {
                    expect(AiringInfo(dict: self.garbage)).toNot(beNil())
                }
                
                it("should have a empty string for time") {
                    expect(AiringInfo(dict: self.garbage)?.time).toNot(beNil())
                    expect(AiringInfo(dict: self.garbage)?.time).to(beEmpty())
                    expect { try AiringInfo(dict: self.garbage)?.time?.toDate() }
                        .to(throwError { (error: String.DateConversionError) in
                            if case .DateNotFound(let message) = error {
                                expect(message).to(equal("Cannot convert an empty string!"))
                            }
                            else if case .BadDate(let message) = error {
                                expect(message).to(equal("The given string, \(testString), is in an incorrect format. It must match the format \(dateFormat)."))
                            }
                            else {
                                fail("Wrong error [\(error)] was thrown!")
                            }
                            })
                }
                
                it("should have a value of 0 for countdown") {
                    expect(AiringInfo(dict: self.garbage)?.countdown).toNot(beNil())
                    expect(AiringInfo(dict: self.garbage)?.countdown).to(equal(0))
                }
                
                it("should have a value of 0 for next episode") {
                    expect(AiringInfo(dict: self.garbage)?.nextEpisode).toNot(beNil())
                    expect(AiringInfo(dict: self.garbage)?.nextEpisode).to(equal(0))
                }
            }
        }
    }
}