//
//  AnalystListStatsTests.swift
//  Analyst
//
//  Created by Nick McVroom-Amoakohene on 20/08/2016.
//  Copyright © 2016 inslayn interactive. All rights reserved.
//

import Quick
import Nimble

@testable import Analyst

class AnalystListStatsTests: QuickSpec {
    let data: [String: AnyObject?] = ["plan_to_watch" : 999, "watching" : 9001, "completed" : 1986, "on_hold" : 3, "dropped" : 7]
    let garbage: [String: AnyObject?] = ["plan_t_watch" : 999, "wathing" : 9001, "compleed" : 1986, "on_old" : 3, "droped" : 7]
    
    override func spec() {
        describe("Analyst List Stats Model Tests") {
            context("if data is empty") {
                it("should return nil") {
                    expect(ListStats(dict: [:])).to(beNil())
                }
            }
            
            context("if data is valid") {
                it("should not return nil") {
                    expect(ListStats(dict: self.data)).toNot(beNil())
                }
                
                it("should have a value for plan_to_watch") {
                    expect(ListStats(dict: self.data)?.planToWatch).toNot(beNil())
                    expect(ListStats(dict: self.data)?.planToWatch).to(beGreaterThanOrEqualTo(0))
                }
                
                it("should have a value for watching") {
                    expect(ListStats(dict: self.data)?.watching).toNot(beNil())
                    expect(ListStats(dict: self.data)?.watching).to(beGreaterThanOrEqualTo(0))
                }
                
                it("should have a value for completed") {
                    expect(ListStats(dict: self.data)?.completed).toNot(beNil())
                    expect(ListStats(dict: self.data)?.completed).to(beGreaterThanOrEqualTo(0))
                }
                
                it("should have a value for on_hold") {
                    expect(ListStats(dict: self.data)?.onHold).toNot(beNil())
                    expect(ListStats(dict: self.data)?.onHold).to(beGreaterThanOrEqualTo(0))
                }
                
                it("should have a value for dropped") {
                    expect(ListStats(dict: self.data)?.dropped).toNot(beNil())
                    expect(ListStats(dict: self.data)?.dropped).to(beGreaterThanOrEqualTo(0))
                }
            }
            
            context("if data is not valid") {
                it("should not return nil") {
                    expect(ListStats(dict: self.garbage)).toNot(beNil())
                }
                
                it("should have a value of 0 for plan_to_watch") {
                    expect(ListStats(dict: self.garbage)?.planToWatch).toNot(beNil())
                    expect(ListStats(dict: self.garbage)?.planToWatch).to(equal(0))
                }
                
                it("should have a value of 0 for watching") {
                    expect(ListStats(dict: self.garbage)?.watching).toNot(beNil())
                    expect(ListStats(dict: self.garbage)?.watching).to(equal(0))
                }
                
                it("should have a value of 0 for completed") {
                    expect(ListStats(dict: self.garbage)?.completed).toNot(beNil())
                    expect(ListStats(dict: self.garbage)?.completed).to(equal(0))
                }
                
                it("should have a value of 0 for on_hold") {
                    expect(ListStats(dict: self.garbage)?.onHold).toNot(beNil())
                    expect(ListStats(dict: self.garbage)?.onHold).to(equal(0))
                }
                
                it("should have a value of 0 for dropped") {
                    expect(ListStats(dict: self.garbage)?.dropped).toNot(beNil())
                    expect(ListStats(dict: self.garbage)?.dropped).to(equal(0))
                }
            }
        }
    }
}