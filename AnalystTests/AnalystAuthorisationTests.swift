//
//  AnalystAuthorisationTests.swift
//  Analyst
//
//  Created by Nick McVroom-Amoakohene on 15/05/2016.
//  Copyright © 2016 inslayn interactive. All rights reserved.
//

import Quick
import Nimble
import Mockingjay
import SwiftyJSON

@testable import Analyst

class AnalystAuthorisationTests: QuickSpec {
    override func spec() {
        self.stub(http(.POST, uri: "/api/auth/access_token"), builder: json(authresponse))
        
        beforeSuite {
            let config = AnalystConfig(clientId: "id", clientSecret: "secret", apiUrl: "https://api.host/api/")
            
            enabledLogLevel = LogLevel.all
            
            expect {
                try AnalystService.initialise(withConfig: config)
            }
            .notTo(throwError())
            
            expect(AniListAuthResponse(json: JSON(nilLiteral: ()))).to(beNil())
            
            expect {
                try AniList.authenticate(with: .Unknown)
            }
            .to(throwError())
        }
        
        beforeEach {
            expect { try AnalystService.authenticateAsGuest() }
                .toNot(throwError())
        }
        
        afterEach {
            AniList.authResponse?.clear()
        }
        
        afterSuite {
            AniList.authResponse?.clear()
        }
        
        describe("Guest Authorisation") {
            context("when authorising as a guest user", {
                
                it("returns an access token", closure: {
                    expect(AniList.authResponse).toEventuallyNot(beNil())
                    expect(AniList.authResponse?.accessToken).toEventuallyNot(beNil())
                })
                
                it("returns an expiry timestamp", closure: {
                    expect(AniList.authResponse).toEventuallyNot(beNil())
                    expect(AniList.authResponse?.expires).toEventuallyNot(beNil())
                })
                
                it("it returns an expiry timestamp in the future", closure: {
                    expect(AniList.authResponse).toEventuallyNot(beNil())
                    
                    if let interval = AniList.authResponse?.expires {
                        let expiry = NSDate(timeIntervalSince1970: NSTimeInterval(interval))
                        expect(expiry.isAfterNow).toEventually(beTrue())
                    }
                })
                
                it("returns an expires duration", closure: {
                    expect(AniList.authResponse).toEventuallyNot(beNil())
                    expect(AniList.authResponse?.expiresIn).toEventuallyNot(beNil())
                })
                
                it("returns an expires duration that is greater than zero", closure: {
                    expect(AniList.authResponse).toEventuallyNot(beNil())
                    expect(AniList.authResponse?.expiresIn).toEventually(beTruthy())
                })
                
                it("does not return a refresh token", closure: {
                    expect(AniList.authResponse).toEventuallyNot(beNil())
                    expect(AniList.authResponse?.refreshToken).toEventually(beNil())
                })
                
                it("does not allow you to request a new token before the old token expires", closure: {
                    expect(AniList.authResponse).toEventuallyNot(beNil())
                    expect(AniList.canAuthenticate()).toEventually(beFalse())
                })
            })
            
            context("when clearing the tokens", {
                it("allows you to request a new token afterwards", closure: {
                    expect(AniList.authResponse).toNot(beNil())
                    expect(AniList.canAuthenticate()).to(beFalse())
                    
                    AnalystService.clearAuthentication()
                    
                    expect(AniList.authResponse).to(beNil())
                    expect(AniList.canAuthenticate()).to(beTrue())
                })
            })
        }
    }
}