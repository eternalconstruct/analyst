//
//  AnalystTimeIntervalExtensionsTests.swift
//  Analyst
//
//  Created by Nick McVroom-Amoakohene on 20/08/2016.
//  Copyright © 2016 inslayn interactive. All rights reserved.
//

import Quick
import Nimble
import Mockingjay

@testable import Analyst

class AnalystTimeIntervalExtensionsTests: QuickSpec {
    override func spec() {
        describe("Testing Time Interval Extensions") {
            context("using NSTimeInterval.asDate extension") {
                it("should return a date for \(testTimeInterval).asDate") {
                    expect(testTimeInterval.asDate).to(beAKindOf(NSDate))
                }
                
                it("should pass because \(testTimeInterval).asDate should be equal to test date \(testDate)") {
                    expect(testTimeInterval.asDate).to(equal(testDate))
                }
                
                it("should pass because negative \(testTimeInterval).asDate should be equal to test date \(testDate)") {
                    expect((-testTimeInterval).asDate).to(equal(testDate))
                }
                
                it("should pass because \(0).asDate should not be equal to test date \(testDate)") {
                    expect(NSTimeInterval(0).asDate).toNot(equal(testDate))
                }
            }
        }
    }
}