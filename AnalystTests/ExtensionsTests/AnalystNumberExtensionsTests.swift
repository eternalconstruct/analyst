//
//  AnalystNumberExtensionsTests.swift
//  Analyst
//
//  Created by Nick McVroom-Amoakohene on 20/08/2016.
//  Copyright © 2016 inslayn interactive. All rights reserved.
//

import Quick
import Nimble
import Mockingjay

@testable import Analyst

class AnalystNumberExtensionsTests: QuickSpec {
    
    override func spec() {
        describe("Testing Number Extensions") {
            context("using Int.asDate extension") {
                it("should return a date for \(testDateUnix).asDate") {
                    expect(testDateUnix.asDate).to(beAKindOf(NSDate))
                }
                
                it("should pass because \(testDateUnix).asDate should be equal to test date \(testDate)") {
                    expect(testDateUnix.asDate).to(equal(testDate))
                }
                
                it("should pass because negative \(testDateUnix).asDate should be equal to test date \(testDate)") {
                    expect((-testDateUnix).asDate).to(equal(testDate))
                }
            }
            
            context("using Int.secondsToDaysHoursMinutesSeconds extension") {
                it("should return 1 second for the value negative 1") {
                    let (days, hours, minutes, seconds) = (-1).secondsToDaysHoursMinutesSeconds
                    
                    expect(days).to(equal(0))
                    expect(hours).to(equal(0))
                    expect(minutes).to(equal(0))
                    expect(seconds).to(equal(1))
                }
                
                it("should return 1 day, 1 hour, 1 minute, 1 second for the value negative 90061") {
                    let (days, hours, minutes, seconds) = (-(1 + 60 + (60 * 60) + (60 * 60 * 24))).secondsToDaysHoursMinutesSeconds
                    
                    expect(days).to(equal(1))
                    expect(hours).to(equal(1))
                    expect(minutes).to(equal(1))
                    expect(seconds).to(equal(1))
                }
                
                it("should return 1 second for the value 1") {
                    let (days, hours, minutes, seconds) = 1.secondsToDaysHoursMinutesSeconds
                    
                    expect(days).to(equal(0))
                    expect(hours).to(equal(0))
                    expect(minutes).to(equal(0))
                    expect(seconds).to(equal(1))
                }
                
                it("should return 1 minute for the value 60") {
                    let (days, hours, minutes, seconds) = 60.secondsToDaysHoursMinutesSeconds
                    
                    expect(days).to(equal(0))
                    expect(hours).to(equal(0))
                    expect(minutes).to(equal(1))
                    expect(seconds).to(equal(0))
                }
                
                it("should return 1 hour for the value 3600") {
                    let (days, hours, minutes, seconds) = (60 * 60).secondsToDaysHoursMinutesSeconds
                    
                    expect(days).to(equal(0))
                    expect(hours).to(equal(1))
                    expect(minutes).to(equal(0))
                    expect(seconds).to(equal(0))
                }
                
                it("should return 1 day for the value 86400") {
                    let (days, hours, minutes, seconds) = (60 * 60 * 24).secondsToDaysHoursMinutesSeconds
                    
                    expect(days).to(equal(1))
                    expect(hours).to(equal(0))
                    expect(minutes).to(equal(0))
                    expect(seconds).to(equal(0))
                }
                
                it("should return 1 day, 1 hour, 1 minute, 1 second for the value 90061") {
                    let (days, hours, minutes, seconds) = (1 + 60 + (60 * 60) + (60 * 60 * 24)).secondsToDaysHoursMinutesSeconds
                    
                    expect(days).to(equal(1))
                    expect(hours).to(equal(1))
                    expect(minutes).to(equal(1))
                    expect(seconds).to(equal(1))
                }
            }
        }
    }
}
