//
//  AnalystSequenceTypeExtensionsTests.swift
//  Analyst
//
//  Created by Nick McVroom-Amoakohene on 20/08/2016.
//  Copyright © 2016 inslayn interactive. All rights reserved.
//

import Quick
import Nimble
import Mockingjay

@testable import Analyst

let sequence = [1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89]

class AnalystSequenceTypeExtensionsTests: QuickSpec {
    
    override func spec() {
        describe("Testing SequenceType Extensions") {
            context("using SequenceType.frequencies extension") {
                it("should not return an empty array") {
                    expect(sequence.frequencies()).toNot(beEmpty())
                }
                
                it("should return same amount of frequencies as numbers in the sequence") {
                    expect(sequence.frequencies().reduce(0) { (accumulator, frequency) in
                            return accumulator! + frequency.1
                        }
                    ).to(equal(sequence.count))
                }
                
                it("should return the value 2 for the frequency of the number 1 in the sequence") {
                    expect(sequence.frequencies().filter({ $0.0 == 1 }).first?.1).to(equal(2))
                }
                
                it("should return frequencies such that no frequency equals 0") {
                    expect(sequence.frequencies().filter({ $0.1 == 0 })).to(beEmpty())
                }
            }
        }
    }
}
