//
//  AnalystStringExtensionsTests.swift
//  Analyst
//
//  Created by Nick McVroom-Amoakohene on 20/08/2016.
//  Copyright © 2016 inslayn interactive. All rights reserved.
//

import Quick
import Nimble
import Mockingjay

@testable import Analyst

class AnalystStringExtensionsTests: QuickSpec {
    
    override func spec() {
        describe("Testing String Extensions") {
            context("using String.toDate extension") {
                it("should not throw an error for \(testDateString).toDate") {
                    expect { try testDateString.toDate() }.toNot(throwError())
                }
                
                it("should return a date for \(testDateString).toDate") {
                    expect { try testDateString.toDate() }.to(beAKindOf(NSDate))
                }
                
                it("should pass because \(testDateString).toDate should be equal to test date \(testDate)") {
                    expect { try testDateString.toDate() }.to(equal(testDate))
                }
                
                it("should pass because \(testOtherDateString).toDate should not be equal to test date \(testDate)") {
                    expect { try testOtherDateString.toDate() }.toNot(equal(testDate))
                }
                
                it("should throw an error for \(testString).toDate") {
                    expect { try testString.toDate() }.to(throwError())
                }
                
                it("should throw an error for emptyString.toDate") {
                    expect { try emptyString.toDate() }.to(throwError())
                }
                
                it("should throw a BadDate error for \(testString).toDate") {
                    expect { try testString.toDate() }
                        .to(throwError { (error: String.DateConversionError) in
                            if case .BadDate(let message) = error {
                                expect(message).to(equal("The given string, \(testString), is in an incorrect format. It must match the format \(dateFormat)."))
                            }
                            else {
                                fail("Wrong error [\(error)] was thrown!")
                            }
                        })
                }
                
                it("should throw a DateNotFound error for emptyString.toDate") {
                    expect { try emptyString.toDate() }
                        .to(throwError { (error: String.DateConversionError) in
                            if case .DateNotFound(let message) = error {
                                expect(message).to(equal("Cannot convert an empty string!"))
                            }
                            else {
                                fail("Wrong error [\(error)] was thrown!")
                            }
                        })
                }
            }
            
            context("using String.first") {
                it("should return M as the first character of \(testString)") {
                    expect(testString.first).to(equal("M"))
                }
                
                it("should return emptyString as the first character of emptyString") {
                    expect(emptyString.first).to(equal(emptyString))
                }
            }
            
            context("using String.last") {
                it("should return t as the last character of \(testString)") {
                    expect(testString.last).to(equal("t"))
                }
                
                it("should return emptyString as the last character of emptyString") {
                    expect(emptyString.last).to(equal(emptyString))
                }
            }
            
            context("using String.uppercaseFirst") {
                it("should return \(testString) as the result of \(lowercaseString).uppercaseFirst") {
                    expect(lowercaseString.uppercaseFirst).to(equal(testString))
                }
                
                it("should return emptyString as the result of emptyString.uppercaseFirst") {
                    expect(emptyString.uppercaseFirst).to(equal(emptyString))
                }
            }
        }
    }
}
