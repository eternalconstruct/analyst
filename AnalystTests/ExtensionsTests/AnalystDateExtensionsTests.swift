//
//  AnalystDateExtensionsTests.swift
//  Analyst
//
//  Created by Nick McVroom-Amoakohene on 20/08/2016.
//  Copyright © 2016 inslayn interactive. All rights reserved.
//

import Quick
import Nimble
import Mockingjay

@testable import Analyst

class AnalystDateExtensionsTests: QuickSpec {
    var today       = NSDate()
    var nextSecond  = NSDate()
    var nextMinute  = NSDate()
    var nextHour    = NSDate()
    var nextDay     = NSDate()
    var nextWeek    = NSDate()
    var nextMonth   = NSDate()
    var nextYear    = NSDate()
    
    override func spec() {
        beforeSuite {
            let components      = NSDateComponents()
            components.second   = 0
            components.minute   = 0
            components.hour     = 0
            components.day      = 7
            components.month    = 3
            components.year     = 2016
            
            self.today          = NSCalendar.currentCalendar().dateFromComponents(components)!
            
            components.second   = 1
            components.minute   = 0
            components.hour     = 0
            components.day      = 7
            components.month    = 3
            components.year     = 2016
            
            self.nextSecond     = NSCalendar.currentCalendar().dateFromComponents(components)!
            
            components.second   = 0
            components.minute   = 1
            components.hour     = 0
            components.day      = 7
            components.month    = 3
            components.year     = 2016
            
            self.nextMinute     = NSCalendar.currentCalendar().dateFromComponents(components)!
            
            components.second   = 0
            components.minute   = 0
            components.hour     = 1
            components.day      = 7
            components.month    = 3
            components.year     = 2016
            
            self.nextHour       = NSCalendar.currentCalendar().dateFromComponents(components)!
            
            components.second   = 0
            components.minute   = 0
            components.hour     = 0
            components.day      = 8
            components.month    = 3
            components.year     = 2016
            
            self.nextDay        = NSCalendar.currentCalendar().dateFromComponents(components)!
            
            components.second   = 0
            components.minute   = 0
            components.hour     = 0
            components.day      = 14
            components.month    = 3
            components.year     = 2016
            
            self.nextWeek       = NSCalendar.currentCalendar().dateFromComponents(components)!
            
            components.second   = 0
            components.minute   = 0
            components.hour     = 0
            components.day      = 7
            components.month    = 4
            components.year     = 2016
            
            self.nextMonth      = NSCalendar.currentCalendar().dateFromComponents(components)!
            
            components.second   = 0
            components.minute   = 0
            components.hour     = 0
            components.day      = 7
            components.month    = 3
            components.year     = 2017
            
            self.nextYear       = NSCalendar.currentCalendar().dateFromComponents(components)!
        }
        
        describe("Testing Date Extensions") {
            context("using NSDate.isBeforeNow extension") {
                it("should return true for \(pastDate).isBeforeNow") {
                    expect(pastDate.isBeforeNow).to(beTrue())
                }
                
                it("should return false for \(futureDate).isBeforeNow") {
                    expect(futureDate.isBeforeNow).to(beFalse())
                }
            }
            
            context("using NSDate.isAfterNow extension") {
                it("should return true for \(futureDate).isAfterNow") {
                    expect(futureDate.isAfterNow).to(beTrue())
                }
                
                it("should return false for \(pastDate).isAfterNow") {
                    expect(pastDate.isAfterNow).to(beFalse())
                }
            }
            
            context("using NSDate.isToday extension") {
                it("should return false for \(futureDate).isToday") {
                    expect(futureDate.isToday).to(beFalse())
                }
                
                it("should return false for \(pastDate).isToday") {
                    expect(pastDate.isToday).to(beFalse())
                }
                
                it("should return true for \(NSDate()).isToday") {
                    expect(NSDate().isToday).to(beTrue())
                }
            }
            
            context("using NSDate.isEqualToDateIgnoringTime extension") {
                it("should return true for \(futureDate).isEqualToDateIgnoringTime \(futureDate)") {
                    expect(futureDate.isEqualToDateIgnoringTime(futureDate)).to(beTrue())
                }
                
                it("should return false for \(pastDate).isEqualToDateIgnoringTime \(futureDate)") {
                    expect(pastDate.isEqualToDateIgnoringTime(futureDate)).to(beFalse())
                }
            }
            
            context("using NSDate.timeUntilDate extension") {
                it("should return 1d for \(tomorrowDate).timeUntilDate") {
                    expect(tomorrowDate.timeUntilDate).to(equal("1d"))
                }
                
                it("should return emptyString for \(yesterdayDate).timeUntilDate") {
                    expect(yesterdayDate.timeUntilDate).to(equal(emptyString))
                }
            }
            
            context("using NSDate.timeSinceDate extension") {
                it("should return 1d for \(yesterdayDate).timeSinceDate") {
                    expect(yesterdayDate.timeSinceDate).to(equal("1d"))
                }
                
                it("should return emptyString for \(tomorrowDate).timeSinceDate") {
                    expect(tomorrowDate.timeSinceDate).to(equal(emptyString))
                }
            }
            
            context("using NSDate.yearsFrom extension") {
                it("should return 1 for \(self.nextYear).yearsFrom \(self.today)") {
                    // nextYear's date should be one year from today
                    expect(self.nextYear.yearsFrom(self.today)).to(equal(1))
                }
            }
            
            context("using NSDate.monthsFrom extension") {
                it("should return 12 for \(self.nextYear).monthsFrom \(self.today)") {
                    // nextYear's date should be 12 months from today
                    expect(self.nextYear.monthsFrom(self.today)).to(equal(12))
                }
            }
            
            context("using NSDate.weeksFrom extension") {
                it("should return 52 for \(self.nextYear).weeksFrom \(self.today)") {
                    // nextYear's date should be 52 weeks from today
                    expect(self.nextYear.weeksFrom(self.today)).to(equal(52))
                }
            }
            
            context("using NSDate.daysFrom extension") {
                it("should return 365 for \(self.nextYear).daysFrom \(self.today)") {
                    // nextYear's date should be 365 days from today
                    expect(self.nextYear.daysFrom(self.today)).to(equal(365))
                }
            }
            
            context("using NSDate.hoursFrom extension") {
                it("should return 365 for \(self.nextYear).hoursFrom \(self.today)") {
                    // nextYear's date should be 365 hours from today
                    expect(self.nextYear.hoursFrom(self.today)).to(equal(8760))
                }
            }
            
            context("using NSDate.minutesFrom extension") {
                it("should return 365 for \(self.nextYear).minutesFrom \(self.today)") {
                    // nextYear's date should be 365 minutes from today
                    expect(self.nextYear.minutesFrom(self.today)).to(equal(525600))
                }
            }
            
            context("using NSDate.secondsFrom extension") {
                it("should return 365 for \(self.nextYear).secondsFrom \(self.today)") {
                    // nextYear's date should be 365 seconds from today
                    expect(self.nextYear.secondsFrom(self.today)).to(equal(31536000))
                }
            }
            
            context("using NSDate.offsetFrom extension") {
                it("should return 1y for \(self.nextYear).offsetFrom \(self.today)") {
                    expect(self.nextYear.offsetFrom(self.today)).to(equal("1y"))
                }
                
                it("should return 1M for \(self.nextMonth).offsetFrom \(self.today)") {
                    expect(self.nextMonth.offsetFrom(self.today)).to(equal("1M"))
                }
                
                it("should return 1w for \(self.nextWeek).offsetFrom \(self.today)") {
                    expect(self.nextWeek.offsetFrom(self.today)).to(equal("1w"))
                }
                
                it("should return 1d for \(self.nextDay).offsetFrom \(self.today)") {
                    expect(self.nextDay.offsetFrom(self.today)).to(equal("1d"))
                }
                
                it("should return 1h for \(self.nextHour).offsetFrom \(self.today)") {
                    expect(self.nextHour.offsetFrom(self.today)).to(equal("1h"))
                }
                
                it("should return 1m for \(self.nextMinute).offsetFrom \(self.today)") {
                    expect(self.nextMinute.offsetFrom(self.today)).to(equal("1m"))
                }
                
                it("should return 1s for \(self.nextSecond).offsetFrom \(self.today)") {
                    expect(self.nextSecond.offsetFrom(self.today)).to(equal("1s"))
                }
            }
            
            context("using NSDate addition operator") {
                it("should pass because \(self.today) add 3600 seconds should equal \(self.nextHour)") {
                    expect(self.today + NSTimeInterval(3600)).to(equal(self.nextHour))
                }
            }
            
            context("using NSDate subtraction operator") {
                it("should pass because \(self.nextHour) subtract 3600 seconds should equal \(self.today)") {
                    expect(self.nextHour - NSTimeInterval(3600)).to(equal(self.today))
                }
            }
            
            context("using NSDate equalTo operator") {
                it("should return true for \(self.today) is equalTo \(self.today)") {
                    expect(self.today == self.today).to(beTrue())
                }
                
                it("should return false for \(self.nextDay) is equalTo \(self.today)") {
                    expect(self.nextDay == self.today).to(beFalse())
                }
            }
            
            context("using NSDate notEqualTo operator") {
                it("should return false for \(self.today) is notEqualTo \(self.today)") {
                    expect(self.today != self.today).to(beFalse())
                }
                
                it("should return true for \(self.nextDay) is notEqualTo \(self.today)") {
                    expect(self.nextDay != self.today).to(beTrue())
                }
            }
            
            context("using NSDate lessThan operator") {
                it("should return false for \(self.today) is lessThan \(self.today)") {
                    expect(self.today < self.today).to(beFalse())
                }
                
                it("should return true for \(self.today) is lessThan \(self.nextDay)") {
                    expect(self.today < self.nextDay).to(beTrue())
                }
            }
            
            context("using NSDate greaterThan operator") {
                it("should return false for \(self.today) is greaterThan \(self.nextDay)") {
                    expect(self.today > self.nextDay).to(beFalse())
                }
                
                it("should return true for \(self.nextDay) is greaterThan \(self.today)") {
                    expect(self.nextDay > self.today).to(beTrue())
                }
            }
            
            context("using NSDate lessThanOrEqualTo operator") {
                it("should return true for \(self.today) is lessThanOrEqualTo \(self.nextDay)") {
                    expect(self.today <= self.nextDay).to(beTrue())
                }
                
                it("should return true for \(self.today) is lessThanOrEqualTo \(self.today)") {
                    expect(self.today <= self.today).to(beTrue())
                }
                
                it("should return false for \(self.nextDay) is lessThanOrEqualTo \(self.today)") {
                    expect(self.nextDay <= self.today).to(beFalse())
                }
            }
            
            context("using NSDate greaterThanOrEqualTo operator") {
                it("should return false for \(self.today) is greaterThanOrEqualTo \(self.nextDay)") {
                    expect(self.today >= self.nextDay).to(beFalse())
                }
                
                it("should return true for \(self.today) is greaterThanOrEqualTo \(self.today)") {
                    expect(self.today >= self.today).to(beTrue())
                }
                
                it("should return true for \(self.nextDay) is greaterThanOrEqualTo \(self.today)") {
                    expect(self.nextDay >= self.today).to(beTrue())
                }
            }
        }
    }
}