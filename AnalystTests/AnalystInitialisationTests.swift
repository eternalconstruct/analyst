//
//  AnalystTests.swift
//  AnalystTests
//
//  Created by Nick McVroom-Amoakohene on 14/05/2016.
//  Copyright © 2016 inslayn interactive. All rights reserved.
//

import Quick
import Nimble

@testable import Analyst

class AnalystInitialisationTests: QuickSpec {
    override func spec() {
        describe("Analyst Initialisation") {
            var config: AnalystConfig!
            
            context("when credentials are correct", closure: {
                it("initialises correctly and returns a valid object", closure: {
                    config = AnalystConfig(clientId: "id", clientSecret: "secret", apiUrl: "https://anilist.co/api/")
                    
                    expect(config.clientId).notTo(beEmpty())
                    expect(config.clientId).notTo(beNil())
                    expect(config.clientSecret).notTo(beEmpty())
                    expect(config.clientSecret).notTo(beNil())
                    expect(config.apiUrl).notTo(beEmpty())
                    expect(config.apiUrl).notTo(beNil())
                    
                    expect {
                        try AnalystService.initialise(withConfig: config)
                    }.notTo(throwError())
                    
                    
                    expect(AnalystService.initialised).toEventually(beTrue())
                })
            })
            
            context("when the client id is empty", closure: {
                it("fails to initialise and returns an error", closure: {
                    config = AnalystConfig(clientId: "", clientSecret: "secret", apiUrl: "https://anilist.co/api/")
                    
                    expect(config.clientId).to(beEmpty())
                    expect(config.clientId).notTo(beNil())
                    expect(config.clientSecret).notTo(beEmpty())
                    expect(config.clientSecret).notTo(beNil())
                    expect(config.apiUrl).notTo(beEmpty())
                    expect(config.apiUrl).notTo(beNil())
                    
                    expect {
                        try AnalystService.initialise(withConfig: config)
                    }.to(throwError{ (error: InitialisationError) in
                        if case .ClientID(let message) = error {
                            expect(message).to(equal("Client ID is not set!"))
                        }
                        else {
                            fail("Wrong error, (\(error)), was thrown!")
                        }
                    })
                })
            })
            
            context("when the client secret is empty", closure: {
                it("fails to initialise and returns an error", closure: {
                    config = AnalystConfig(clientId: "id", clientSecret: "", apiUrl: "https://anilist.co/api/")
                    
                    expect(config.clientId).notTo(beEmpty())
                    expect(config.clientId).notTo(beNil())
                    expect(config.clientSecret).to(beEmpty())
                    expect(config.clientSecret).toNot(beNil())
                    expect(config.apiUrl).notTo(beEmpty())
                    expect(config.apiUrl).notTo(beNil())
                    
                    expect {
                        try AnalystService.initialise(withConfig: config)
                    }.to(throwError{ (error: InitialisationError) in
                        if case .ClientSecret(let message) = error {
                            expect(message).to(equal("Client Secret is not set!"))
                        }
                        else {
                            fail("Wrong error, (\(error)), was thrown!")
                        }
                    })
                })
            })
            
            context("when the API Url is empty", closure: {
                it("fails to initialise and returns an error", closure: {
                    config = AnalystConfig(clientId: "id", clientSecret: "secret", apiUrl: "")
                    
                    expect(config.clientId).notTo(beEmpty())
                    expect(config.clientId).notTo(beNil())
                    expect(config.clientSecret).notTo(beEmpty())
                    expect(config.clientSecret).notTo(beNil())
                    expect(config.apiUrl).to(beEmpty())
                    expect(config.apiUrl).notTo(beNil())
                    
                    expect {
                        try AnalystService.initialise(withConfig: config)
                    }.to(throwError{ (error: InitialisationError) in
                        if case .APIUrl(let message) = error {
                            expect(message).to(equal("API Root URL is not set!"))
                        }
                        else {
                            fail("Wrong error, (\(error)), was thrown!")
                        }
                    })
                })
            })
            
            context("when credentials are incorrect", closure: {
                it("fails to initialise and returns an error", closure: {
                    config = AnalystConfig(clientId: "wrong_client_id", clientSecret: "wrong_client_secret", apiUrl: "https://anilist.co/api/")
                    
                    expect(config.clientId).notTo(beEmpty())
                    expect(config.clientId).notTo(beNil())
                    expect(config.clientSecret).notTo(beEmpty())
                    expect(config.clientSecret).notTo(beNil())
                    expect(config.apiUrl).notTo(beEmpty())
                    expect(config.apiUrl).notTo(beNil())
                    
                    expect {
                        try AnalystService.initialise(withConfig: config)
                    }.to(throwError{ (error: InitialisationError) in
                        if case .Failed(let message) = error {
                            expect(message).to(equal("Initialisation failed due to incorrect credentials!"))
                        }
                        else {
                            fail("Wrong error, (\(error)), was thrown!")
                        }
                    })
                })
            })
            
            context("when the API Url is incorrect", closure: {
                it("fails to initialise and returns an error", closure: {
                    config = AnalystConfig(clientId: "id", clientSecret: "secret", apiUrl: "wrong_api_url")
                    
                    expect(config.clientId).notTo(beEmpty())
                    expect(config.clientId).notTo(beNil())
                    expect(config.clientSecret).notTo(beEmpty())
                    expect(config.clientSecret).notTo(beNil())
                    expect(config.apiUrl).notTo(beEmpty())
                    expect(config.apiUrl).notTo(beNil())
                    
                    expect {
                        try AnalystService.initialise(withConfig: config)
                    }.to(throwError{ (error: InitialisationError) in
                        if case .Failed(let message) = error {
                            expect(message).to(equal("Initialisation failed, probably an incorrect API Url!"))
                        }
                        else {
                            fail("Wrong error, (\(error)), was thrown!")
                        }
                    })
                })
            })
        }
    }
}


