//
//  AnalystHelperData.swift
//  Analyst
//
//  Created by Nick McVroom-Amoakohene on 17/05/2016.
//  Copyright © 2016 inslayn interactive. All rights reserved.
//

import Foundation

let emptyString         = ""
let testString          = "Monkeys are great"
let lowercaseString     = "monkeys are great"
let dateFormat          = "yyyy-MM-dd'T'HH:mm:ssZZZZ"
let testDateString      = "2001-09-09T01:46:40Z"
let testOtherDateString = "1002-09-09T10:46:04Z"
let testDate            = try! testDateString.toDate()
let testDateUnix        = 1000000000
let testTimeInterval    = NSTimeInterval(1000000000.0)

let yesterdayDate   = NSDate(timeIntervalSinceNow: -NSTimeInterval(60 * 60 * 24))

// add 1 hour to ensure it checks the whole interval
let tomorrowDate    = NSDate(timeIntervalSinceNow: NSTimeInterval(60 * 60 * 25))

let pastDate        = NSDate.distantPast()
let futureDate      = NSDate.distantFuture()

let timestamp: Int = Int(NSDate().dateByAddingTimeInterval(NSTimeInterval(3600)).timeIntervalSince1970)

let authresponse: [String:AnyObject] = [
    "access_token":"fake_access_token",
    "expires":timestamp,
    "expires_in":3600,
    "token_type":"bearer"
]

let authorizeResponse: [String:AnyObject] = [
    "url": NSURL(string: "app://authorisation?code=fake_authorisation_code")!
]

let authresponseWithRefreshToken: [String:AnyObject] = [
    "access_token":"fake_access_token",
    "expires":timestamp,
    "expires_in":3600,
    "token_type":"bearer",
    "refresh_token":"fake_refresh_token"
]

let smallAnimeModel: [String:AnyObject] = ["id":1,"title_romaji":"Cowboy Bebop","type":"TV","image_url_med":"http://anilist.co/img/dir/anime/med/1.jpg","image_url_sml":"http://anilist.co/img/dir/anime/sml/1.jpg","title_japanese":"カウボーイビバップ","title_english":"Cowboy Bebop","synonyms":[],"image_url_lge":"http://aniist.co/img/dir/anime/reg/1.jpg","airing_status":"finished airing","average_score":"86.8","total_episodes":26,"adult":false,"popularity":7574,"relation_type":NSNull(),"role":NSNull()]

//let animeModel = ["id":21,"title_romaji":"One Piece","type":"TV","image_url_med":"http://anilist.co/img/dir/anime/med/21.jpg","image_url_sml":"http://anilist.co/img/dir/anime/sml/21.jpg","start_date":"1999-10-20T21:00:00+09:00","end_date":NSNull(),"classification":"PG-13 - Teens 13 or older","hashtag":"","source":"","adult":false,"popularity":9802,"title_japanese":"ワンピース","title_english":"One Piece","synonyms":[],"description":"Gol D. Roger was known as the Pirate King, the strongest and most infamous being to have sailed the Grand Line. The capture and death of Roger by the World Government brought a change throughout the world. His last words before his death revealed the location of the greatest treasure in the world, One Piece. It was this revelation that brought about the Grand Age of Pirates, men who dreamed of finding One Piece (which promises an unlimited amount of riches and fame), and quite possibly the most coveted of titles for the person who found it, the title of the Pirate King.<br><br>\r\nEnter Monkey D. Luffy, a 17-year-old boy that defies your standard definition of a pirate. Rather than the popular persona of a wicked, hardened, toothless pirate who ransacks villages for fun, Luffy’s reason for being a pirate is one of pure wonder; the thought of an exciting adventure and meeting new and intriguing people, along with finding One Piece, are his reasons of becoming a pirate. Following in the footsteps of his childhood hero, Luffy and his crew travel across the Grand Line, experiencing crazy adventures, unveiling dark mysteries and battling strong enemies, all in order to reach One Piece.<br><br>\r\n[Written by MAL Rewrite]","image_url_lge":"http://anilist.co/img/dir/anime/reg/21.jpg","image_url_banner":"http://anilist.co/img/dir/anime/banner/21.jpg","duration":24,"airing_status":"currently airing","average_score":"83.4","total_episodes":0,"youtube_id":NSNull(),"relation_type":NSNull(),"role":NSNull(),"list_stats":["plan_to_watch":643,"watching":5864,"completed":135,"on_hold":1865,"dropped":1295],"genres":["Action","Adventure","Comedy","Drama","Fantasy","Shounen"],"airing":["time":"2016-05-22T09:30:00+09:00","countdown":436468,"next_episode":742]]