//
//  AnalystTests.swift
//  Analyst
//
//  Created by Nick McVroom-Amoakohene on 29/05/2016.
//  Copyright © 2016 inslayn interactive. All rights reserved.
//

import Quick
import Nimble
import Mockingjay

@testable import Analyst

class AnalystTests: QuickSpec {
//    override func spec() {
//        beforeSuite {
//            let config = AnalystConfig(clientId: "id", clientSecret: "secret", apiUrl: "https://api.host/api/")
//            
//            enabledLogLevel = LogLevel.all
//            var data: NSData?
//            var model: AnyObject?
//            
//            do {
//                let jsonData = try NSJSONSerialization.dataWithJSONObject(smallAnimeModel, options: NSJSONWritingOptions.PrettyPrinted)
//                data = jsonData;
//                // here "jsonData" is the dictionary encoded in JSON data
//            } catch let error as NSError {
//                print(error)
//            }
//            
//            do {
//                let decoded = try NSJSONSerialization.JSONObjectWithData(data!, options: [])// as? [String:String]
//                // here "decoded" is the dictionary decoded from JSON data
//                model = decoded
//            } catch let error as NSError {
//                print(error)
//            }
//            
//            self.stub(http(.GET, uri: "https://api.host/api/anime/1"), builder: json(model!))
//            
//            self.stub(http(.POST, uri: "/api/auth/access_token"), builder: json(authresponse))
//            
//            expect {
//                try Analyst.instance.initialise(withConfig: config)
//                }.notTo(throwError())
//            
//            expect {
//                try Analyst.instance.authorise(with: .ClientCredentials)
//                }.toNot(throwError())
//        }
//        
//        describe("Anime call with id 1") {
//            it("returns an Anime object") {
//                var anime: Anime?
//                
//                Analyst.instance.anime("1")?.onSuccess({ entity in
//                    anime = entity.content as? Anime
//                })
//                
//                expect(anime).toEventuallyNot(beNil())
//                expect(anime!.id).toEventually(equal(1))
//            }
//        }
//    }
}
